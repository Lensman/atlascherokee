﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RESTAMISvc
{
    public class PlayerManagement
    {
        #region Public methods
        internal static Messages360.msgPlayerRestrictions getPlayerRestrictions(int PlayerID)
        {
            Messages360.msgPlayerRestrictions resp = new Messages360.msgPlayerRestrictions();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = PlayerManagement.GetPlayerRestrictions(PlayerID);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerRestrictions { Success = false, Msg = "Unable to get Player restrictions." };
            }
        }

        internal static Messages360.msgPlayerCommentsResponse getPlayerComments(int PlayerID, bool ShowAll)
        {
            Messages360.msgPlayerCommentsResponse resp = new Messages360.msgPlayerCommentsResponse();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp.Comments = GetPlayerComments(PlayerID, ShowAll);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                resp.Success = true;
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerCommentsResponse { Success = false, Msg = "Unable to get Player comments." };
            }
        }

        #endregion

        #region Private methods
        private static List<Messages360.CommentItem> GetPlayerComments(int PlayerID, bool ShowAll)
        {
            List<Messages360.CommentItem> bComments = null;

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    cn.Open();

                    SqlCommand cmd = new SqlCommand("usp_PlayerComments", cn);
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", Value = PlayerID });

                    SqlCommand cmd2 = new SqlCommand("usp_CommentVisibility", cn);
                    cmd2.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", Value = PlayerID });

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        if (bComments == null) bComments = new List<Messages360.CommentItem>();
                        Messages360.CommentItem ci = new Messages360.CommentItem
                        {
                            Comment = reader["Comment"].ToString(),
                            CommentNum = Convert.ToInt32(reader["CommentNumber"]),
                            DateEntered = Convert.ToDateTime(reader["[Date]"]),
                            EnteredBy = reader["Firstname"].ToString() + " " + reader["Lastname"],
                            Priority = reader["Priority"].ToString(),
                            Private = reader["Private"].ToString(),
                            Status = reader["Status"].ToString()
                        };

                        if (reader["Expiration"] != DBNull.Value) ci.Expiration = Convert.ToDateTime(reader["Expiration"]);

                        if (ShowAll || (ci.Private.ToLower() == "n" && (!ci.Expiration.HasValue || ci.Expiration.Value >= DateTime.Now)))
                        {
                            cmd2.Parameters.Add(new SqlParameter { ParameterName = "@CmtNum", Value = ci.CommentNum });
                            SqlDataReader reader2 = cmd2.ExecuteReader();
                            while (reader2.Read())
                            {
                                ci.CommentType += reader2["Description"].ToString() + ", ";
                            }
                        }

                        ci.CommentType = ci.CommentType.Substring(0, ci.CommentType.LastIndexOf(","));
                        bComments.Add(ci);
                    }
                    cmd.Dispose();
                    cn.Close();

                    return bComments.OrderBy(comm => comm.Priority).ThenByDescending(comm => comm.DateEntered).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Messages360.msgPlayerRestrictions GetPlayerRestrictions(int PlayerID)
        {
            Messages360.msgPlayerRestrictions resp = new Messages360.msgPlayerRestrictions();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_PlayerRestrictions", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", SqlDbType = System.Data.SqlDbType.Int, Value = PlayerID });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    resp.Success = true;
                    resp.Msg = "No restrictions";

                    while (reader.Read())
                    {
                        if (resp.Restrictions == null) resp.Restrictions = new List<Messages360.Restriction>();
                        resp.Restrictions.Add(new Messages360.Restriction
                        {
                            Code = reader["RestrictionMaskID"].ToString(),
                            Description = reader["Description"].ToString()
                        });
                        resp.Msg = string.Empty;
                    }
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}