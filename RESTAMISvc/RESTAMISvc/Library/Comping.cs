﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RESTAMISvc
{
    public class Comping
    {
        internal static Messages360.msgCompHistory GetCompHistory(int PlayerID, int period)
        {
            Messages360.msgCompHistory resp = new Messages360.msgCompHistory();
            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_PlayerCompHistory", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", SqlDbType = System.Data.SqlDbType.Int, Value = PlayerID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@LastNDays", SqlDbType = System.Data.SqlDbType.Int, Value = period });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    resp.CompHistory = new List<Messages360.CompHistoryItem>();
                    DateTime tPostDT;
                    int tInt = 0;
                    while (reader.Read())
                    {
                        if (DateTime.TryParse(reader["Issued Date"].ToString(), out tPostDT))
                        {
                            if (tPostDT >= DateTime.Now.AddDays(period * -1))
                            {
                                Messages360.CompHistoryItem thisComp = new Messages360.CompHistoryItem();
                                thisComp.AuthorizerLogin = reader["Authorized By"].ToString();
                                thisComp.CompDescription = reader["Description"].ToString();
                                thisComp.CompID = Convert.ToInt32(reader["CompID"]);
                                thisComp.IssueDT = tPostDT;
                                thisComp.FreeCompType = reader["Type"].ToString();
                                thisComp.IssuedBy = reader["Issued By"].ToString();
                                thisComp.Status = reader["Status"].ToString();
                                if (DateTime.TryParse(reader["Redeemed Date"].ToString(), out tPostDT))
                                    thisComp.PostedDate = tPostDT;
                                thisComp.Quantity = Convert.ToInt32(reader["Covers"]);
                                thisComp.PointValue = Convert.ToInt32(reader["Points"]);
                                thisComp.RetailValue = Convert.ToDecimal(reader["Comp"]);
                                if (int.TryParse(reader["Rewards"].ToString(), out tInt))
                                    thisComp.DaysValid = tInt;
                                else
                                    thisComp.DaysValid = 0;
                                resp.CompHistory.Add(thisComp);
                            }
                        }

                    }
                    if (resp.CompHistory.Count > 0)
                        resp.CompHistory = resp.CompHistory.OrderByDescending(x => x.IssueDT).ToList();

                    resp.Success = true;
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}