﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RESTAMISvc
{
    public class MiscFunctions
    {
        #region Public methods
        internal static Messages360.msgPlayerIDFound getPlayerIDFromCardID(string cardID)
        {
            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                int iPlayer = GetPlayerIDFromCardID(cardID);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return new Messages360.msgPlayerIDFound { PlayerID = iPlayer, Success = true };
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerIDFound { Success = false, Msg = "Unable to get PlayerID from Card swipe." };
            }
        }
        #endregion

        #region Private methods
        private static int GetPlayerIDFromCardID(string CardID)
        {
            int iPlayerID = 0;
            int? tiPlayerID;
            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.REPLPMDB))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.fn_PlayerIDGet(@CardID, true)", cn);
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@CardID", Value = CardID });
                    tiPlayerID = (int?)cmd.ExecuteScalar();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return iPlayerID;
        }

        internal static string GetCountryFromStateCode(string StateCode)
        {
            string sResult = string.Empty;
            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.REPLPMDB))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT TOP 1 CountryID FROM dbo.States (NOLOCK) WHERE State1 = @StateCode");
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@StateCode", Value = StateCode });
                    sResult = cmd.ExecuteScalar().ToString();
                    cn.Close();
                }

                if (string.IsNullOrEmpty(sResult))
                {
                    sResult = "NONE";
                }
                return sResult;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal static Messages360.msgIGTUserInfo GetABSUserID(int HostLoginID, bool UseDev)
        {
            Messages360.msgIGTUserInfo resp = new Messages360.msgIGTUserInfo();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_GetABSUser", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = System.Data.SqlDbType.VarChar, Value = HostLoginID });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        resp.userInfo = new Messages360.IGTUserInfo
                        {
                            Firstname = reader["Firstname"].ToString(),
                            LastName = reader["Lastname"].ToString(),
                            LoginName = reader["LoginName"].ToString(),
                            UserID = Convert.ToInt32(reader["UserID"])
                        };
                        resp.Success = true;
                    }
                    resp.Success = true;
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}