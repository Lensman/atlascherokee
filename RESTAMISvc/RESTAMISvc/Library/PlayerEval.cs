﻿using AtlasCore;
using RESTAMISvc.Models;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace RESTAMISvc
{
    public class PlayerEval
    {
        #region Public methods
        internal static Messages360.msgPlayerEval getPlayerEval(int PlayerID, DateTime StartDT, DateTime EndDT)
        {
            Messages360.msgPlayerEval resp = new Messages360.msgPlayerEval();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = PlayerEval.getPlayerEval(PlayerID, StartDT, EndDT);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerEval { Success = false, Msg = "Unable to get Player restrictions." };
            }
        }

        internal static Messages360.msgPlayerYTD getPlayerTripYTD(string PlayerID)
        {
            Messages360.msgPlayerYTD resp = new Messages360.msgPlayerYTD();
            CRMAcresMessage lresp = new CRMAcresMessage();

            CRMAcresMessage req = new CRMAcresMessage();
            req.Header = new Header();
            req.Header.MessageID = AtlasRandom.GetRandom(1, 10000);
            req.Header.TimeStamp = DateTime.Now;
            req.Header.Operation = new HeaderOperation();
            req.Header.Operation.Data = HeaderOperationData.TripSummary;
            req.Header.Operation.DataSpecified = true;
            req.Header.Operation.Operand = HeaderOperationOperand.Request;
            req.Header.Operation.OperandSpecified = true;
            req.PlayerID = PlayerID;

            ErrorLogHelper err = new ErrorLogHelper();

            try
            {
                //serialize the object
                string serMsg = AtlasCore.Utils.XMLSerializeToString(req);

                //If verbose logging is on, write the serialized XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, serMsg, Settings.Default.appName,
                        System.Diagnostics.EventLogEntryType.Information);

                //call AMI passing the serialized data to the HTTP Adapter
                Stopwatch tmr = Stopwatch.StartNew();
                string sRet = Utils.executeProc(Settings.Default.IGTHttpEndpoint, serMsg);
                tmr.Stop();
                Utils.LogElapsed(err, MethodBase.GetCurrentMethod().Name, serMsg, tmr.ElapsedMilliseconds);

                //If verbose logging is on, write the response XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, sRet, Settings.Default.appName,
                        System.Diagnostics.EventLogEntryType.Information);

                //deserialize the response string back into the appropriate class
                lresp = AtlasCore.Utils.XMLDeserializeFromString<CRMAcresMessage>(sRet);

                if (lresp.Header.Operation.Data == HeaderOperationData.TripSummary &&
                    lresp.Header.Operation.Operand == HeaderOperationOperand.Information)
                {
                    string tAmount = string.Empty;
                    // good response, parse the data.
                    BodyTripSummary ts = (BodyTripSummary)lresp.Body.Items[0];
                    foreach (BodyTripSummarySummary tss in ts.Items)
                    {
                        if (tss.SummaryType == BodyTripSummarySummarySummaryType.YTD)
                        {
                            // report only whole dollar amount so drop last two digits
                            resp.PointsEarned = tss.PointsEarned;
                            if (tss.PointsEarned >= 100)
                            {
                                tAmount = tss.PointsEarned.ToString();
                                tAmount = tAmount.Substring(0, tAmount.Length - 2);
                                resp.DollarsEarned = Convert.ToInt32(tAmount);
                            }
                            else
                            {
                                resp.DollarsEarned = 0;
                            }
                            resp.TotalTrips = 1;
                        }
                    }
                    resp.Success = true;
                }
                else if (lresp.Header.Operation.Operand == HeaderOperationOperand.Error)
                {
                    //  Process the error.
                    BodyError error = (BodyError)lresp.Body.Items[0];
                    resp.Success = false;
                    resp.Msg = error.ErrorDescription;
                }

                return resp;
            }
            catch (Exception ex)
            {
                err.WriteErrorLogEntry(err.GetProcName(), ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerYTD { Success = false, Msg = "Unable to get Player YTD info." };
            }
        }

        internal static Messages360.msgTrips getPlayerTrip(string PlayerID, int numTrips)
        {
            Messages360.msgTrips resp = new Messages360.msgTrips();
            CRMAcresMessage lresp = new CRMAcresMessage();

            CRMAcresMessage req = new CRMAcresMessage();
            req.Header = new Header();
            req.Header.MessageID = AtlasRandom.GetRandom(1, 10000);
            req.Header.TimeStamp = DateTime.Now;
            req.Header.Operation = new HeaderOperation();
            req.Header.Operation.Data = HeaderOperationData.TripInformation;
            req.Header.Operation.DataSpecified = true;
            req.Header.Operation.Operand = HeaderOperationOperand.Request;
            req.Header.Operation.OperandSpecified = true;
            req.PlayerID = PlayerID;

            if (numTrips > 0)
            {
                req.Body = new Body();
                BodyTripInformation[] tsr = new BodyTripInformation[1];
                tsr[0] = new BodyTripInformation();
                BodyTripInformationTripFilter[] tf = new BodyTripInformationTripFilter[1];
                tf[0] = new BodyTripInformationTripFilter();
                tf[0].Site = new SiteType { SiteID = 1 };
                tf[0].IncludeLinks = true;
                tf[0].MaxTrips = numTrips;
                tf[0].MaxTripsSpecified = true;
                tsr[0].Items = tf;
                req.Body.Items = tsr;
            }

            ErrorLogHelper err = new ErrorLogHelper();

            try
            {
                //serialize the object
                string serMsg = AtlasCore.Utils.XMLSerializeToString(req);

                //If verbose logging is on, write the serialized XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, serMsg, Settings.Default.appName, System.Diagnostics.EventLogEntryType.Information);

                //call AMI passing the serialized data to the HTTP Adapter
                Stopwatch tmr = Stopwatch.StartNew();
                string sRet = Utils.executeProc(Settings.Default.IGTHttpEndpoint, serMsg);
                tmr.Stop();
                Utils.LogElapsed(err, MethodBase.GetCurrentMethod().Name, serMsg, tmr.ElapsedMilliseconds);

                //If verbose logging is on, write the response XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, sRet, Settings.Default.appName, System.Diagnostics.EventLogEntryType.Information);

                //deserialize the response string back into the appropriate class
                lresp = AtlasCore.Utils.XMLDeserializeFromString<CRMAcresMessage>(sRet);

                if (lresp.Header.Operation.Data == HeaderOperationData.TripInformation &&
                    lresp.Header.Operation.Operand == HeaderOperationOperand.Information)
                {
                    string tAmount = string.Empty;
                    // good response, instantiate response sub objects and parse the data.
                    resp.Trips = new List<Messages360.TripInstance>();
                    if (lresp.Body != null && lresp.Body.Items.Length > 0 && ((BodyTripInformation)lresp.Body.Items[0]).Items != null)
                    {
                        BodyTripInformation ts = (BodyTripInformation)lresp.Body.Items[0];
                        foreach (BodyTripInformationTripDetail tss in ts.Items)
                        {
                            Messages360.TripInstance ti = new Messages360.TripInstance
                            {
                                ActualCompGuideline = tss.ActualCompGuideline,
                                ActualIncome = tss.ActualIncome,
                                PointsEarned = tss.PointsEarned,
                                TheoCompGuideline = tss.TheoCompGuideline,
                                TheoIncome = tss.TheoIncome,
                                TotalExpAsPercentTheo = tss.TotalExpAsPercentTheo,
                                TotalExpAsPercentActual = tss.TotalExpAsPercentActual,
                                BeginDate = tss.BeginDate,
                                Days = tss.Days,
                                TripNumber = tss.TripNumber
                            };

                            ti.SlotStatistics = new Messages360.SlotStats
                            {
                                ActWin = tss.SlotStatistics.ActWin,
                                CoinIn = tss.SlotStatistics.CoinIn,
                                CoinOut = tss.SlotStatistics.CoinOut,
                                ExtraCreditUsed = tss.SlotStatistics.XCUsed,
                                Jackpots = tss.SlotStatistics.Jackpots,
                                PointsEarned = tss.SlotStatistics.PointsEarned,
                                TheoWin = tss.SlotStatistics.TheoWin
                            };

                            ti.TableStatistics = new Messages360.TableStats
                            {
                                ActWin = tss.TableStatistics.ActWin,
                                AvgBet = tss.TableStatistics.AvgBet,
                                CashBuyIn = tss.TableStatistics.CashBuyIn,
                                FourHrConv = tss.TableStatistics.FourHrConv,
                                NetMarkers = tss.TableStatistics.NetMarkers,
                                PointsEarned = tss.TableStatistics.PointsEarned,
                                TheoWin = tss.TableStatistics.TheoWin,
                                TimePlayed = tss.TableStatistics.TimePlayed
                            };

                            ti.Revenue = new Messages360.RevenueItem
                            {
                                ActWin = tss.Revenue.ActWin,
                                TheoWin = tss.Revenue.TheoWin
                            };

                            ti.Expenses = new List<Messages360.ExpenseItem>();
                            foreach (BodyTripInformationTripDetailExpensesExpense ee in tss.Expenses.ExpenseEntries)
                            {
                                ti.Expenses.Add(new Messages360.ExpenseItem
                                {
                                    ExpType = ee.Type,
                                    Amount = ee.Amount
                                });
                            }

                            resp.Trips.Add(ti);
                        }

                        resp.Success = true;
                    }
                    else
                    {
                        resp.Success = false;
                        resp.Msg = Settings.Default.msgNoTrips;
                    }
                }
                else if (lresp.Header.Operation.Operand == HeaderOperationOperand.Error)
                {
                    //  Process the error.
                    BodyError error = (BodyError)lresp.Body.Items[0];
                    resp.Success = false;
                    resp.Msg = error.ErrorDescription;
                }

                return resp;
            }
            catch (Exception ex)
            {
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgTrips { Success = false, Msg = "Unable to get Player trips." };
            }
        }
        #endregion

        #region Private methods
        private static Messages360.msgPlayerEval GetPlayerEval(int PlayerID, DateTime StartDT, DateTime EndDT)
        {
            Messages360.msgPlayerEval resp = new Messages360.msgPlayerEval();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.REPLPMDB))
                {

                    SqlCommand cmd = new SqlCommand("usp_PlayerEval", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@StartDT", DbType = System.Data.DbType.DateTime, Value = StartDT });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@EndDT", DbType = System.Data.DbType.DateTime, Value = EndDT });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        resp.GrossTheoWin = Convert.ToDecimal(reader["Gross TheoWin"]);
                        resp.GrossActualWin = Convert.ToDecimal(reader["Gross ActualWin"]);
                        resp.DaysPlayed = Convert.ToInt32(reader["Days Played"]);
                        resp.FSPUsed = Convert.ToDecimal(reader["FSPUsed"]);
                        resp.ResortDollarsUsed = Convert.ToDecimal(reader["ResDollUsed"]);
                        resp.CompsUsed = Convert.ToDecimal(reader["CompsUsed"]);
                        resp.GrossADT = Convert.ToDecimal(reader["Gross ADT"]);
                        resp.GrossADA = Convert.ToDecimal(reader["Gross ADA"]);
                        resp.GrossADW = Convert.ToDecimal(reader["Gross ADW"]);
                        resp.TotalReinvestment = Convert.ToDecimal(reader["Total Reinvest"]);
                        resp.TheoReinvestmentPct = Convert.ToDecimal(reader["Theo Reinvest %"]);
                        resp.ActualReinvestmentPct = Convert.ToDecimal(reader["Actual Reinvest %"]);
                        resp.OverallCompStatus = reader["Overall Status"].ToString();
                        resp.CompStatusMsg = reader["Status Message"].ToString();
                        resp.PreferredGameType = reader["Game Preference"].ToString();
                        resp.Success = true;
                    }
                    else
                    {
                        resp.Success = false;
                        resp.Msg = "No matching guests found.";
                    }
                    cmd.Dispose();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resp;
        }

        private static Messages360.msgTripsDateRange GetTripDateRange(int PlayerID, Messages360.EvalPeriod Period)
        {
            try
            {
                Messages360.msgTripsDateRange resp = new Messages360.msgTripsDateRange();
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_TripDateRange", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", SqlDbType = System.Data.SqlDbType.Int, Value = PlayerID });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@QueryType",
                        SqlDbType = System.Data.SqlDbType.Char,
                        Size = 1,
                        Value = (Period == Messages360.EvalPeriod.Lifetime ? 'A' : Period == Messages360.EvalPeriod.LastTrip ? 'L' : 'P')
                    });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        resp.StartDT = Convert.ToDateTime(reader["StartDate"]);
                        resp.EndDT = Convert.ToDateTime(reader["EndDate"]);
                        resp.Success = true;
                    }
                    resp.Success = true;
                }
                return resp;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}