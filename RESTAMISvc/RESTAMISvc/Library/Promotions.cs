﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RESTAMISvc
{
    public class Promotions
    {
        const int PromoStatusInvited = 1;
        const int PromoStatusCancelled = 3;
        const int PromoStatusPending = 4;
        const int PromoStatusPlaying = 5;

        #region Public methods
        internal static Messages360.msgPlayerPromos getPlayerPromos(int PlayerID, bool InternetOnly)
        {
            Messages360.msgPlayerPromos resp = new Messages360.msgPlayerPromos();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = Promotions.GetPlayerPromos(PlayerID, InternetOnly);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerPromos { Success = false, Msg = "Unable to get Player promos." };
            }
        }

        internal static Messages360.msgPromoCodeEventDetailsResponse getPlayerPromosWithEvents(int PlayerID, int PromoID, bool InternetOnly)
        {
            Messages360.msgPromoCodeEventDetailsResponse resp = new Messages360.msgPromoCodeEventDetailsResponse();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = Promotions.GetPlayerPromosWithEvents(PlayerID, PromoID, InternetOnly);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPromoCodeEventDetailsResponse { Success = false, Msg = "Unable to get Player promos." };
            }
        }

        #endregion

        #region Private methods
        private static Messages360.msgPlayerPromos GetPlayerPromos(int PlayerID, bool InternetOnly)
        {
            Messages360.msgPlayerPromos resp = new Messages360.msgPlayerPromos();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.LIVEPMDB))
                {
                    string sSQL = "SELECT StartDate, EndDate, PublicDescription, InternetDisplayText, InternetDisplay, PromoName, PromoID, [Status] ";
                    sSQL += "FROM vw_PlayerPromos ";
                    sSQL += "WHERE playerID = @PlayerID AND EndaDate >= @EndDate";

                    SqlCommand cmd = new SqlCommand(sSQL, cn);
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@EndDate", DbType = System.Data.DbType.DateTime, Value = DateTime.Now });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        resp.Promos = new List<Messages360.PromoItem>();
                        if (!InternetOnly || (InternetOnly && reader["InternetDisplay"].ToString().ToUpper() == "Y"))
                        {
                            resp.Promos.Add(new Messages360.PromoItem
                            {
                                BeginDate = Convert.ToDateTime(reader["BeginDate"]),
                                Description = (reader["InternetDisplayText"] == DBNull.Value) ? reader["PublicDesc"].ToString() : reader["InternetDisplayText"].ToString(),
                                EndDate = Convert.ToDateTime(reader["EndDate"]),
                                PromoID = Convert.ToInt32(reader["PromoID"]),
                                PromoName = reader["PromoName"].ToString(),
                                PublicDescription = reader["PublicDescription"].ToString(),
                                InternetDescription = reader["InternetDisplayText"].ToString(),
                                Status = reader["Status"].ToString()
                            });
                        }
                    }
                    if (resp.Promos == null)
                    {
                        resp.Success = false;
                        resp.Msg = Settings.Default.msgNoPromos;
                    }
                    else
                        resp.Success = true;

                    return resp;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Messages360.msgPromoCodeEventDetailsResponse GetPlayerPromosWithEvents(int PlayerID, int PromoID, bool InternetOnly)
        {
            Messages360.msgPromoCodeEventDetailsResponse resp = new Messages360.msgPromoCodeEventDetailsResponse();

            int curPromoID = 0;
            int curEventID = 0;
            int iInternetOnly = 0;
            string sProcName = string.Empty;

            if (InternetOnly) iInternetOnly = 1;
            else iInternetOnly = 0;

            if (PromoID > 0) sProcName = "[base].[Proc_PlayerManagement_PromoEventByPlayerPromo_Get]";
            else sProcName = "[base].[Proc_PlayerManagement_PromoEventByPlayer_Get]";
            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.LIVEPMDB))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand(sProcName, cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@@PlayerID", SqlDbType = System.Data.SqlDbType.Int, SqlValue = PlayerID });

                    if (PromoID > 0)
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@@PromoID", SqlDbType = System.Data.SqlDbType.Int, SqlValue = PromoID });

                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@@InternetOnly", SqlDbType = System.Data.SqlDbType.TinyInt, SqlValue = iInternetOnly });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@@SiteID", SqlDbType = System.Data.SqlDbType.SmallInt, SqlValue = 1 });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@@sqlInt", SqlDbType = System.Data.SqlDbType.SmallInt, SqlValue = 0 });
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        resp.PromosWithEvents = new List<Messages360.PromoDetail>();
                        Messages360.PromoDetail pd = null;
                        Messages360.promoCodeEventDtl pe = null;
                        Messages360.promoCodeEventBlockDtl pb = null;

                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["PromoID"]) != curPromoID)
                            {
                                if (pd != null)
                                {
                                    pd.PromoEvents.Add(pe);
                                    pe = null;
                                    resp.PromosWithEvents.Add(pd);
                                }
                                pd = new Messages360.PromoDetail();
                                pd.PromoEvents = new List<Messages360.promoCodeEventDtl>();
                                pe = null;
                                pb = null;
                                curPromoID = Convert.ToInt32(reader["PromoID"]);
                                pd.PromoID = curPromoID;
                                pd.LongDescription = reader["PromoInternetDisplayText"].ToString();
                                pd.StartDate = Convert.ToDateTime(reader["StartDate"]);
                                pd.EndDate = Convert.ToDateTime(reader["EndDate"]);
                                pd.CMSID = reader["PromoName"].ToString();
                            }

                            if (Convert.ToInt32(reader["PromoEventID"]) != curEventID)
                            {
                                if (pe != null) pd.PromoEvents.Add(pe);
                                pe = new Messages360.promoCodeEventDtl();
                                pe.PromoEventBlocks = new List<Messages360.promoCodeEventBlockDtl>();
                                pb = null;
                                curEventID = Convert.ToInt32(reader["PromoEventID"]);
                                pe.PromoEventID = curEventID;
                                pe.PromoEventDescription = reader["PromoEventDescription"].ToString();
                            }

                            pb = new Messages360.promoCodeEventBlockDtl();
                            pb.BlockDescription = reader["PromoEventBlockDescription"].ToString();
                            pb.BlockID = Convert.ToInt32(reader["PromoEventBlockID"]);
                            pb.BlockIssuedTickets = Convert.ToInt32(reader["PromoEventBlockIssuedTickets"]);
                            pb.BlockPlayerIssuedTickets = Convert.ToInt32(reader["PromoEventBlockIssuedPlayerTickets"]);
                            pb.BlockTickets = Convert.ToInt32(reader["PromoEventBlockTickets"]);
                            pb.MaxTicketsPerPlayer = Convert.ToInt32(reader["PromoEventBlockMaxPerPlayer"]);
                            if (Utils.DerivedDateTime(pb.BlockDescription, pd.StartDate) >= DateTime.Now.Date)
                                pe.PromoEventBlocks.Add(pb);
                        }

                        if (pd.PromoEvents.Count == 0 && pe != null)
                        {
                            pd.PromoEvents.Add(pe);
                            resp.PromosWithEvents.Add(pd);
                        }
                    }
                }
                resp.Success = true;
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}