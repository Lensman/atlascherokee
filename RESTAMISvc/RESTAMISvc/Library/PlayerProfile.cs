﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RESTAMISvc
{
    public class PlayerProfile
    {
        #region Public methods
        internal static Messages360.msgPlayerProfile getPlayerProfile(int PlayerID)
        {
            Messages360.msgPlayerProfile resp = new Messages360.msgPlayerProfile();

            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = PlayerProfile.GetQuickPlayerProfile(PlayerID);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerProfile { Success = false, Msg = "Unable to get Player profile." };
            }
        }
        #endregion

        #region Private methods
        private static Messages360.msgPlayerProfile GetQuickPlayerProfile(int PlayerID)
        {
            Messages360.msgPlayerProfile resp = new Messages360.msgPlayerProfile();
            string tString = string.Empty;
            bool GuestFound = true;

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.REPLPMDB))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_QuickProfile", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });

                    SqlDataReader reader = cmd.ExecuteReader();

                    // returns multiple result sets.  the first is player name

                    while (reader.Read())
                    {
                        if (resp.NameList == null) resp.NameList = new List<Messages360.PlayerName>();
                        Messages360.PlayerName gn = new Messages360.PlayerName();
                        gn.FirstName = reader["PlayerName"].ToString();
                        resp.NameList.Add(gn);

                        DateTime? tDOB = (DateTime)reader["DOB"];
                        if (tDOB.HasValue)
                            resp.DOB = tDOB.Value.ToString("d");

                        string credStat = reader["CreditStatus"].ToString();
                        if (string.IsNullOrEmpty(credStat))
                            resp.CustStatus = reader["CustStatus"].ToString();
                        else
                            resp.CustStatus = credStat;

                        resp.Gender = reader["Gender"].ToString();

                        resp.HostName = reader["HostName"].ToString();
                    }

                    GuestFound = (resp.NameList != null);

                    if (GuestFound)
                    {
                        // Next result set is addresses
                        reader.NextResult();

                        while (reader.Read())
                        {
                            if (resp.Addresses == null) resp.Addresses = new List<Messages360.PlayerAddress>();
                            Messages360.PlayerAddress ga = new Messages360.PlayerAddress();
                            ga.Addr1 = reader["Line1"].ToString();
                            ga.Addr2 = reader["Line2"].ToString();
                            ga.City = reader["City"].ToString();
                            ga.State = reader["State"].ToString();
                            ga.ZipCode = reader["ZipCode"].ToString();
                            ga.AddressName = reader["Description"].ToString();
                            ga.MailingAddress = (reader["Mailing"].ToString().ToLower() == "y");
                            resp.Addresses.Add(ga);
                        }

                        // Next are phone numbers
                        reader.NextResult();

                        while (reader.Read())
                        {
                            if (resp.PhoneNumbers == null) resp.PhoneNumbers = new List<Messages360.PlayerPhone>();
                            Messages360.PlayerPhone gp = new Messages360.PlayerPhone();
                            gp.Number = reader["Phone"].ToString();
                            gp.Location = reader["Description"].ToString();
                            gp.Preferred = (reader["Preferred"].ToString().ToLower() == "y");
                            resp.PhoneNumbers.Add(gp);
                        }

                        // Final result set is emails
                        reader.NextResult();

                        while (reader.Read())
                        {
                            if (reader["DefaultEmail"].ToString().ToLower() == "y")
                                resp.PreferredEmailAddress = reader["Email"].ToString();
                            else
                                resp.EmailAddress = reader["Email"].ToString();
                        }
                        resp.Success = true;
                    }
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}