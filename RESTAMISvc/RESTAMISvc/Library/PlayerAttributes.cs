﻿using AtlasCore;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RESTAMISvc
{
    public class PlayerAttributes
    {
        #region Public methods
        internal static Messages360.msgPlayerBalance getPlayerBalances(int PlayerID)
        {
            Messages360.msgPlayerBalance resp = new Messages360.msgPlayerBalance();
            try
            {
                Stopwatch tmr = Stopwatch.StartNew();
                resp = PlayerAttributes.GetPlayerBalances(PlayerID);
                tmr.Stop();
                Utils.LogElapsed(MethodBase.GetCurrentMethod().Name, "get", tmr.ElapsedMilliseconds);
                return resp;
            }
            catch (Exception ex)
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerBalance { Success = false, Msg = "Unable to get Player balances." };
            }
        }
        #endregion

        #region Private methods
        private static Messages360.msgPlayerBalance GetPlayerBalances(int PlayerID)
        {
            Messages360.msgPlayerBalance resp = new Messages360.msgPlayerBalance();
            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.REPLPMDB))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_QuickBalance", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string tbal = reader["Points"].ToString();
                        decimal tval = 0;
                        if (decimal.TryParse(tbal, out tval))
                            resp.PointBalance = tval;
                        else
                            resp.PointBalance = 0;

                        tbal = reader["Xtra"].ToString();
                        if (decimal.TryParse(tbal, out tval))
                            resp.XtraLocal = tval;
                        else
                            resp.XtraLocal = 0;
                    }
                    resp.Success = true;
                }

                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Messages360.msgEarnings GetPlayerEarnings(int PlayerID, Messages360.EarningsType eType, int numTrips)
        {
            // The PlayerDay table in the PlayerManagement IGT DB has detailed earnings info.  Here we grab that and return based on the type of
            // query the user selected.  The ADI Trip Info message will not return current (open) trip numbers so getting info directly from the DB
            // is necessary.

            Messages360.msgEarnings resp = new Messages360.msgEarnings();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (eType == Messages360.EarningsType.Trip)
                    {
                        resp.EarnType = Messages360.EarningsType.Trip;
                        cmd.CommandText = "usp_PlayerEarningsByTrips";
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@NumTrips", DbType = System.Data.DbType.Int32, Value = numTrips });
                    }
                    else
                    {
                        resp.EarnType = Messages360.EarningsType.CurDay;
                        cmd.CommandText = "usp_PlayerEarningsByDate";
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@StartDT", DbType = System.Data.DbType.DateTime, Value = DateTime.Now.Date });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@EndDT", DbType = System.Data.DbType.DateTime, Value = DateTime.Now.Date });
                    }

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    int pTableSum = 0;
                    int pSlotSum = 0;
                    while (reader.Read())
                    {
                        if (resp.Values == null) resp.Values = new List<Messages360.DayEarning>();

                        resp.Values.Add(new Messages360.DayEarning
                        {
                            TripNum = Convert.ToInt32(reader["TripNumber"]),
                            SlotPoints = Convert.ToInt32(reader["Slot_PointsEarned"]) + pSlotSum,
                            TablePoints = Convert.ToInt32(reader["Table_PointsEarned"]) + pTableSum,
                            PokerPoints = pTableSum + pSlotSum,
                            AccountingDate = Convert.ToDateTime(reader["AccountingDate"])
                        });
                    }
                    resp.PlayerID = PlayerID;
                    resp.Success = true;
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Messages360.msgEarnings GetPlayerEarnings(int PlayerID, DateTime StartDT, DateTime EndDT)
        {
            // The PlayerDay table in the PlayerManagement IGT DB has detailed earnings info.  Here we grab that and return based on the type of
            // query the user selected.  The ADI Trip Info message will not return current (open) trip numbers so getting info directly from the DB
            // is necessary.

            Messages360.msgEarnings resp = new Messages360.msgEarnings();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_PlayerEarningsByDate", cn);
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", DbType = System.Data.DbType.Int32, Value = PlayerID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@StartDT", DbType = System.Data.DbType.DateTime, Value = StartDT });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@EndDT", DbType = System.Data.DbType.DateTime, Value = EndDT });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    int pTableSum = 0;
                    int pSlotSum = 0;
                    while (reader.Read())
                    {
                        if (resp.Values == null) resp.Values = new List<Messages360.DayEarning>();

                        resp.Values.Add(new Messages360.DayEarning
                        {
                            TripNum = Convert.ToInt32(reader["TripNumber"]),
                            SlotPoints = Convert.ToInt32(reader["Slot_PointsEarned"]) + pSlotSum,
                            TablePoints = Convert.ToInt32(reader["Table_PointsEarned"]) + pTableSum,
                            PokerPoints = pTableSum + pSlotSum,
                            AccountingDate = Convert.ToDateTime(reader["AccountingDate"])
                        });
                    }
                    resp.PlayerID = PlayerID;
                    resp.Success = true;
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Messages360.msgCreditInfo GetCreditInfo(int PlayerID)
        {
            try
            {
                Messages360.msgCreditInfo resp = new Messages360.msgCreditInfo();
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    SqlCommand cmd = new SqlCommand("usp_CreditInfoView", cn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@PlayerID", SqlDbType = System.Data.SqlDbType.Int, Value = PlayerID });

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string tPlayer = reader["Account"].ToString();
                        if (Helpers.isNumeric(tPlayer, System.Globalization.NumberStyles.Integer))
                        {
                            if (resp.CreditItems == null) resp.CreditItems = new List<Messages360.CreditItem>();
                            Messages360.CreditItem ci = new Messages360.CreditItem();
                            ci.PlayerID = Convert.ToInt32(tPlayer);
                            ci.Available = (reader["Available"] != DBNull.Value ? Convert.ToDecimal(reader["Available"]) : 0);
                            ci.CreditLine = (reader["Credit Line"] != DBNull.Value ? Convert.ToDecimal(reader["Credit Line"]) : 0);
                            ci.FrontMoney = (reader["Front Money"] != DBNull.Value ? Convert.ToDecimal(reader["Front Money"]) : 0);
                            ci.InTransit = (reader["In Transit"] != DBNull.Value ? Convert.ToDecimal(reader["In Transit"]) : 0);
                            ci.LastMarker = (reader["Last Marker"] != DBNull.Value ? Convert.ToDateTime(reader["Last Marker"]).ToString() : string.Empty);
                            ci.Outstanding = (reader["Outstanding"] != DBNull.Value ? Convert.ToDecimal(reader["Outstanding"]) : 0);
                            ci.Primary = reader["Primary"].ToString();
                            ci.SiteID = (reader["SiteID"] != DBNull.Value ? Convert.ToInt32(reader["SiteID"]) : 0);
                            ci.SiteName = reader["Site"].ToString();
                            ci.StopCodes = reader["Stop Codes"].ToString();
                            ci.TTO = reader["TTO"].ToString();
                            resp.CreditItems.Add(ci);
                            resp.Success = true;
                        }
                        else
                        {
                            resp.Success = false;
                            resp.Msg = "Not a credit Player.";
                        }
                    }
                }
                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}