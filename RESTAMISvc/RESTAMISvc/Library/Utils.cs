﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using RESTAMISvc.Properties;
using AtlasCore;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using RESTAMISvc.Models;
using System.Xml;

namespace RESTAMISvc
{
    public class Utils
    {
        enum LookupType { PlayerID, Email, DLNo };
        static string evtSource = Settings.Default.appName;

        #region ADI Base Interface
        public static string executeProc(string sURL, string sXML)
        {
            HttpWebRequest oHttpReq;
            HttpWebResponse oResp;
            DateTime startDT = DateTime.Now;
            DateTime endDT = DateTime.Now;
            string strTTag = "";
            string sInputXML = sXML;

            ErrorLogHelper eLog = new ErrorLogHelper(Settings.Default.UtilDB);

            try
            {
                oHttpReq = (HttpWebRequest)WebRequest.Create(sURL);
                oHttpReq.KeepAlive = false;

                Uri oURI = new Uri(sURL);

                oHttpReq.Method = "POST";

                if (!string.IsNullOrEmpty(sXML))
                {
                    strTTag = sXML.Substring(1, sXML.IndexOf(" ") - 1);

                    //sXML = AddEnvelope(sXML);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] byte1 = encoding.GetBytes(sXML);

                    Stream newStream = oHttpReq.GetRequestStream();
                    newStream.Write(byte1, 0, byte1.Length);
                    newStream.Close();
                }
                else
                    throw new FormatException("XML cannot be NULL");

                oResp = (HttpWebResponse)oHttpReq.GetResponse();
                if (oResp.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader oReader = new StreamReader(oResp.GetResponseStream());

                    sXML = oReader.ReadToEnd();
                    if (!String.IsNullOrEmpty(sXML))
                    {
                        sXML = FixSpecialXMLChars(sXML);

                        if (RespContainsError(sXML))
                            eLog.WriteErrorLogEntry(sURL + ";" + eLog.GetProcName(), sXML, evtSource, EventLogEntryType.Error);
                    }
                    oHttpReq = null;
                }
                else
                {
                    sXML = FixSpecialXMLChars(sXML);

                    if (RespContainsError(sXML))
                        eLog.WriteErrorLogEntry(sURL + ";" + eLog.GetProcName(),
                            sXML, evtSource, EventLogEntryType.Error);
                }
                return sXML;
            }
            catch (Exception ex)
            {
                sXML = "<Error ErrNo=\"" + ex.ToString() + "\" ErrDesc=\"" + ex.Message + "\"/>";
                eLog.WriteErrorLogEntry(sURL + "; " + eLog.GetProcName(), ex.Message, evtSource,
                    EventLogEntryType.Error);
                return sXML;
            }
        }

        private static string AddEnvelope(string sXML)
        {
            string MsgPrefix = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            string MsgSuffix = "";
            return MsgPrefix + sXML + MsgSuffix;
        }

        public static string FixSpecialXMLChars(string sXML)
        {
            StringBuilder sb = new StringBuilder(sXML);

            sb.Replace("&amp;", "&");
            sb.Replace("&lt;", "<");
            sb.Replace("&gt;", ">");
            sb.Replace("&quot;", "\"\"");
            sb.Replace(" xmlns=\"http://tempuri.org/\"", "");

            sb.Replace("0x1e", "&gt;");
            sb.Replace("0x1c", "&lt;");
            sb.Replace("&", "+");

            return sb.ToString();
        }

        private static bool RespContainsError(string sResp)
        {
            if (sResp.Contains("(404) Not Found") ||
                sResp.Contains("(403) Forbidden") ||
                sResp.Contains("Unable to connect"))
                return true;
            else
                return false;
        }
        #endregion

        #region Public methods

        internal static DateTime DerivedDateTime(string desc, DateTime StartDT)
        {
            DateTime dResult = Convert.ToDateTime("2014-01-01");

            // Get date and time fields
            string[] descflds = desc.Split(new char[] { ' ' });

            // Create Date
            if (desc.Contains(@"/")) // this is a date and time field
            {
                if (DateTime.TryParse(descflds[0] + "/" + DateTime.Now.Year.ToString(), out dResult))
                {
                    dResult = dResult.Date;
                }
                else
                {
                    dResult = DateTime.Now.AddDays(1).Date;
                }
            }
            else if (desc.ToLower().Contains("pm") || desc.ToLower().Contains("am"))  // time only field
            {
                dResult = StartDT.Date;
            }

            return dResult;
        }

        internal static Messages360.msgPlayersFound findGuests(string FName, string LName, string City, string State, string Zip, DateTime? DOB, string DLNo)
        {
            string bDay = string.Empty;
            Messages360.msgPlayersFound resp = new Messages360.msgPlayersFound();
            resp.Players = new List<Messages360.FoundPlayer>();

            try
            {
                using (SqlConnection cn = new SqlConnection(Settings.Default.UtilDB))
                {
                    string strSQL = "SELECT playerID, Birthday, Status, FirstName, MiddleName, LastName, State, Zipcode, IDNumber FROM base.vw_FindPlayerBase WHERE";

                    if (!string.IsNullOrEmpty(LName)) strSQL += " LastName like '" + LName + "%'";
                    if (!string.IsNullOrEmpty(FName)) strSQL += " AND FirstName like '" + FName + "%'";
                    if (!string.IsNullOrEmpty(State)) strSQL += " AND State = '" + State.ToUpper() + "'";
                    if (!string.IsNullOrEmpty(DLNo)) strSQL += " AND IDNumber = '" + DLNo.ToUpper() + "'";
                    if (!string.IsNullOrEmpty(Zip)) strSQL += " AND Zipcode like '" + Zip + "%'";

                    SqlCommand cmd = new SqlCommand(strSQL, cn);
                    cmd.CommandType = System.Data.CommandType.Text;

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        DateTime tDate;

                        while (reader.Read())
                        {
                            bDay = reader["Birthday"].ToString();
                            if (!DateTime.TryParse(bDay, out tDate))
                                tDate = Convert.ToDateTime("01/01/1801");

                            resp.Players.Add(new Messages360.FoundPlayer
                            {
                                City = reader["Zipcode"].ToString(),
                                DOB = tDate,
                                FirstName = reader["FirstName"].ToString() + (reader["MiddleName"] != DBNull.Value ? " " + reader["MiddleName"].ToString() : string.Empty),
                                LastName = reader["LastName"].ToString(),
                                PlayerID = reader["PlayerID"].ToString(),
                                State = reader["State"].ToString(),
                                Status = reader["Status"].ToString()
                            });
                            if (resp.Players.Count >= Settings.Default.MaxFoundPlayers) break;
                        }

                        if (resp.Players.Count >= Settings.Default.MaxFoundPlayers)
                        {
                            resp.Players = null;
                            resp.Success = false;
                            resp.Msg = Settings.Default.msgCommonName;
                        }
                        else
                        {
                            resp.Success = true;
                        }
                    }
                    else
                    {
                        resp.Success = false;
                        resp.Msg = "No matching guests found.";
                    }
                    cmd.Dispose();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resp;
        }

        internal static void LogElapsed(string module, string operation, long iElapsed)
        {
            if (Settings.Default.perfLogging.ToLower() == "yes")
            {
                ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);
                err.WriteErrorLogEntry(module, string.Format("{0} - {1}ms elapsed", operation, iElapsed), Settings.Default.appName, EventLogEntryType.Information);
            }
        }

        internal static void LogElapsed(ErrorLogHelper err, string module, string operation, long iElapsed)
        {
            if (Settings.Default.perfLogging.ToLower() == "yes")
            {
                err.WriteErrorLogEntry(module, string.Format("{0} - {1}ms elapsed", operation, iElapsed), Settings.Default.appName, EventLogEntryType.Information);
            }

        }

        internal static string serializeAcres(CRMAcresMessage req)
        {
            XmlSerializer ser = new XmlSerializer(typeof(CRMAcresMessage));
            StringBuilder sb = new StringBuilder();

            XmlWriterSettings oSettings = new XmlWriterSettings();
            oSettings.OmitXmlDeclaration = false;
            oSettings.Indent = false;
            oSettings.Encoding = Encoding.UTF8;

            XmlWriter writer = XmlTextWriter.Create(sb, oSettings);
            ser.Serialize(writer, req);
            //stringbuilder sb contains the serialized content

            //do not need the text writer anymore
            writer.Close();
            return sb.ToString();
        }

        internal static CRMAcresMessage deserializeAcres(string sMsg)
        {
            //deserialize the response string back into the appropriate class
            XmlSerializer serOutput = new XmlSerializer(typeof(CRMAcresMessage));
            return (CRMAcresMessage)serOutput.Deserialize(new StringReader(sMsg));
        }

        #endregion

        #region Private Methods

        #endregion

    }
}