﻿using AtlasCore;
using RESTAMISvc.Models;
using RESTAMISvc.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace RESTAMISvc
{
    public class Coupons
    {
        internal static Messages360.msgPlayerCoupons GetPlayerCoupons(string PlayerID)
        {
            Messages360.msgPlayerCoupons resp = new Messages360.msgPlayerCoupons();
            resp.Coupons = new List<Messages360.CouponItem>();
            CRMAcresMessage lresp = new CRMAcresMessage();

            CRMAcresMessage req = new CRMAcresMessage();
            req.Header = new Header();
            req.Header.MessageID = AtlasRandom.GetRandom(1, 10000);
            req.Header.MessageIDSpecified = true;
            req.Header.TimeStamp = DateTime.Now;
            req.Header.Operation = new HeaderOperation();
            req.Header.Operation.Data = HeaderOperationData.CouponListPlayer;
            req.Header.Operation.DataSpecified = true;
            req.Header.Operation.Operand = HeaderOperationOperand.Request;
            req.Header.Operation.OperandSpecified = true;
            req.Header.Operation.Command = "Update";
            req.Header.Operation.WhereClause = "PlayerID = " + PlayerID + " AND EarnSiteID = 1";
            req.Header.Operation.MaxRecords = 1000;

            ErrorLogHelper err = new ErrorLogHelper(Settings.Default.UtilDB);

            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(CRMAcresMessage));
                StringBuilder sb = new StringBuilder();

                XmlWriterSettings oSettings = new XmlWriterSettings();
                oSettings.OmitXmlDeclaration = true;
                oSettings.Indent = false;
                oSettings.Encoding = Encoding.UTF8;

                XmlWriter writer = XmlTextWriter.Create(sb, oSettings);
                ser.Serialize(writer, req);
                //stringbuilder sb contains the serialized content

                //do not need the text writer anymore
                writer.Close();

                //If verbose logging is on, write the serialized XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, sb.ToString(), Settings.Default.appName,
                        System.Diagnostics.EventLogEntryType.Information);

                //create data access class and execute pass the serialized data to the HTTP Adapter
                Stopwatch tmr = Stopwatch.StartNew();
                string sRet = Utils.executeProc(Settings.Default.IGTHttpEndpoint, sb.ToString());
                tmr.Stop();
                Utils.LogElapsed(err, MethodBase.GetCurrentMethod().Name, sb.ToString(), tmr.ElapsedMilliseconds);

                //If verbose logging is on, write the response XML to the event log
                if (Settings.Default.verboseLog.ToLower() == "yes")
                    err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, sRet, Settings.Default.appName,
                        System.Diagnostics.EventLogEntryType.Information);

                //deserialize the response string back into the appropriate structure (class)
                XmlSerializer serOutput = new XmlSerializer(typeof(CRMAcresMessage));
                lresp = (CRMAcresMessage)serOutput.Deserialize(new StringReader(sRet));

                if (lresp.Header.Operation.Data == HeaderOperationData.CouponListPlayer &&
                    lresp.Header.Operation.Operand == HeaderOperationOperand.Information &&
                    lresp.Header.Operation.TotalRecords > 0)
                {
                    // good response, parse the data.
                    foreach (object c in lresp.Body.Items)
                    {
                        Messages360.CouponItem cp = new Messages360.CouponItem
                        {
                            CouponDescription = ((BodyCouponListPlayer)c).CouponItem,
                            CouponName = ((BodyCouponListPlayer)c).CouponDescription,
                            Status = ((BodyCouponListPlayer)c).CouponStatus
                        };

                        cp.BeginDate = ((BodyCouponListPlayer)c).StartDate;
                        if (((BodyCouponListPlayer)c).ExpirationDate != null)
                            cp.EndDate = ((BodyCouponListPlayer)c).ExpirationDate;

                        resp.Coupons.Add(cp);
                    }
                    resp.Success = true;
                }
                else if (lresp.Header.Operation.TotalRecords == 0)
                {
                    resp.Success = true;
                    resp.Msg = "Player has no coupons.";
                }
                else if (lresp.Header.Operation.Operand == HeaderOperationOperand.Error)
                {
                    //  Process the error.
                    BodyError error = (BodyError)lresp.Body.Items[0];
                    resp.Success = false;
                    resp.Msg = error.ErrorDescription;
                }

                return resp;
            }
            catch (Exception ex)
            {
                err.WriteErrorLogEntry(MethodBase.GetCurrentMethod().Name, ex.Message, Settings.Default.appName, EventLogEntryType.Error);
                return new Messages360.msgPlayerCoupons { Success = false, Msg = "Unable to get Player coupons." };
            }
        }
    }
}