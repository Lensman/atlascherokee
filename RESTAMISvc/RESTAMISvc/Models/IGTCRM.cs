namespace RESTAMISvc.Models
{
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
    public partial class CRMAcresMessage
    {
        
        /// <remarks/>
        public Header Header;
        
        /// <remarks/>
        public string PlayerID;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public Body Body;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal SchemaVersion;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SchemaVersionSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
    public partial class Header
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string FromURI;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string ToURI;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string ReplyToURI;
        
        /// <remarks/>
        public int MessageID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MessageIDSpecified;
        
        /// <remarks/>
        public int OriginalMessageID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OriginalMessageIDSpecified;
        
        /// <remarks/>
        public System.DateTime TimeStamp;
        
        /// <remarks/>
        public string Token;
        
        /// <remarks/>
        public string PropertyID;
        
        /// <remarks/>
        public HeaderOperation Operation;
        
        /// <remarks/>
        public string Information;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ImmediateResponseRequired;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImmediateResponseRequiredSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool OriginalBodyRequested;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OriginalBodyRequestedSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class HeaderOperation
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public HeaderOperationOperand Operand;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OperandSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public HeaderOperationData Data;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Command;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string WhereClause;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int MaxRecords;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxRecordsSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int TotalRecords;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalRecordsSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public HeaderOperationSuccess Success;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SuccessSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum HeaderOperationOperand
    {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Update,
        
        /// <remarks/>
        Delete,
        
        /// <remarks/>
        Request,
        
        /// <remarks/>
        Information,
        
        /// <remarks/>
        Success,
        
        /// <remarks/>
        Validate,
        
        /// <remarks/>
        Lock,
        
        /// <remarks/>
        Create,
        
        /// <remarks/>
        Error,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum HeaderOperationData
    {
        
        /// <remarks/>
        AbsUser,
        
        /// <remarks/>
        AddressType,
        
        /// <remarks/>
        Affiliation,
        
        /// <remarks/>
        CardPrint,
        
        /// <remarks/>
        Comments,
        
        /// <remarks/>
        Comp,
        
        /// <remarks/>
        CompCenter,
        
        /// <remarks/>
        CompHistory,
        
        /// <remarks/>
        CompIssuance,
        
        /// <remarks/>
        CompList,
        
        /// <remarks/>
        CompListMessage,
        
        /// <remarks/>
        CompListType,
        
        /// <remarks/>
        CompRedemption,
        
        /// <remarks/>
        CountryInfo,
        
        /// <remarks/>
        CouponIssuance,
        
        /// <remarks/>
        CouponList,
        
        /// <remarks/>
        CouponListPlayer,
        
        /// <remarks/>
        CouponRedemption,
        
        /// <remarks/>
        CreditConditions,
        
        /// <remarks/>
        CreditInfo,
        
        /// <remarks/>
        EmailType,
        
        /// <remarks/>
        Error,
        
        /// <remarks/>
        FindPlayer,
        
        /// <remarks/>
        GamingProfile,
        
        /// <remarks/>
        General,
        
        /// <remarks/>
        Itinerary,
        
        /// <remarks/>
        Language,
        
        /// <remarks/>
        LinkInfo,
        
        /// <remarks/>
        LoginModify,
        
        /// <remarks/>
        Merge,
        
        /// <remarks/>
        Messages,
        
        /// <remarks/>
        PhoneType,
        
        /// <remarks/>
        PINInfo,
        
        /// <remarks/>
        PlayerActivity,
        
        /// <remarks/>
        PlayerBalanceAdjustment,
        
        /// <remarks/>
        PlayerBalances,
        
        /// <remarks/>
        PlayerXtraCreditBalances,
        
        /// <remarks/>
        PlayerCard,
        
        /// <remarks/>
        PlayerCompListMessage,
        
        /// <remarks/>
        PlayerFind,
        
        /// <remarks/>
        PlayerFind2,
        
        /// <remarks/>
        PlayerGroupAdd,
        
        /// <remarks/>
        PlayerGroupLookup,
        
        /// <remarks/>
        PlayerHost,
        
        /// <remarks/>
        PlayerImage,
        
        /// <remarks/>
        PlayerInterests,
        
        /// <remarks/>
        PlayerPIN,
        
        /// <remarks/>
        PlayerProfile,
        
        /// <remarks/>
        PlayerPromoEvent,
        
        /// <remarks/>
        PromoEventTicketIssuance,
        
        /// <remarks/>
        PlayerPromos,
        
        /// <remarks/>
        PlayerPromotions,
        
        /// <remarks/>
        PlayerRanking,
        
        /// <remarks/>
        PlayerReservation,
        
        /// <remarks/>
        PlayerRestrictions,
        
        /// <remarks/>
        PlayerStatus,
        
        /// <remarks/>
        PointsConversionRate,
        
        /// <remarks/>
        Promo,
        
        /// <remarks/>
        PromoWithFilter,
        
        /// <remarks/>
        PromotionAwardsIssuance,
        
        /// <remarks/>
        PromotionAwardsList,
        
        /// <remarks/>
        PromotionAwardsListPlayer,
        
        /// <remarks/>
        PromoAwardPlayerBalance,
        
        /// <remarks/>
        PromotionPlayerStatusUpdate,
        
        /// <remarks/>
        PromotionListSystem,
        
        /// <remarks/>
        PromotionListPlayer,
        
        /// <remarks/>
        PromoEvent,
        
        /// <remarks/>
        PromoEventsWithFilter,
        
        /// <remarks/>
        PromoEventBlock,
        
        /// <remarks/>
        PromoEventBlocksWithFilter,
        
        /// <remarks/>
        PromoMaster,
        
        /// <remarks/>
        PromoStatus,
        
        /// <remarks/>
        PromoType,
        
        /// <remarks/>
        RateRestriction,
        
        /// <remarks/>
        RateType,
        
        /// <remarks/>
        Rating,
        
        /// <remarks/>
        Reason,
        
        /// <remarks/>
        ReservationType,
        
        /// <remarks/>
        Restrictions,
        
        /// <remarks/>
        RoomRestriction,
        
        /// <remarks/>
        RoomType,
        
        /// <remarks/>
        SiteInfo,
        
        /// <remarks/>
        StopCodes,
        
        /// <remarks/>
        TripComment,
        
        /// <remarks/>
        TripInformation,
        
        /// <remarks/>
        TripSummary,
        
        /// <remarks/>
        UnMerge,
        
        /// <remarks/>
        UpdatePlayer,
        
        /// <remarks/>
        ZipCode,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum HeaderOperationSuccess
    {
        
        /// <remarks/>
        Yes,
        
        /// <remarks/>
        No,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=true)]
    public partial class RoomReservation
    {
        
        /// <remarks/>
        public string Action;
        
        /// <remarks/>
        public string SiteID;
        
        /// <remarks/>
        public string Confirmation;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime Arrive;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime Depart;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime ETA;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ETASpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime ETD;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ETDSpecified;
        
        /// <remarks/>
        public RoomReservationRoomAuthorizer RoomAuthorizer;
        
        /// <remarks/>
        public int RoomTypeID;
        
        /// <remarks/>
        public int People;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PeopleSpecified;
        
        /// <remarks/>
        public int Rooms;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RoomsSpecified;
        
        /// <remarks/>
        public string RoomStatus;
        
        /// <remarks/>
        public string RoomNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("NightlyDetail", IsNullable=false)]
        public RoomReservationNightlyDetail[] NightlyDetails;
        
        /// <remarks/>
        public string CRMGUID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class RoomReservationRoomAuthorizer
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=true)]
    public partial class User
    {
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UserIDSpecified;
        
        /// <remarks/>
        public string LoginName;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string License;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class RoomReservationNightlyDetail
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime Night;
        
        /// <remarks/>
        public int RateTypeID;
        
        /// <remarks/>
        public decimal Rate;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=true)]
    public partial class PromoStatus
    {
        
        /// <remarks/>
        public int StatusNumber;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int Type;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TypeSpecified;
        
        /// <remarks/>
        public string Send;
        
        /// <remarks/>
        public string Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=true)]
    public partial class Name
    {
        
        /// <remarks/>
        public string Title;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string PreferredName;
        
        /// <remarks/>
        public string MiddleName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string Suffix;
        
        /// <remarks/>
        public string Generation;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("Site", Namespace="", IsNullable=true)]
    public partial class SiteType
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteIDSpecified;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
    public partial class Body
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AbsUser", typeof(BodyAbsUser))]
        [System.Xml.Serialization.XmlElementAttribute("AddressType", typeof(BodyAddressType))]
        [System.Xml.Serialization.XmlElementAttribute("Affiliation", typeof(BodyAffiliation))]
        [System.Xml.Serialization.XmlElementAttribute("CardPrint", typeof(BodyCardPrint))]
        [System.Xml.Serialization.XmlElementAttribute("Comments", typeof(BodyComments))]
        [System.Xml.Serialization.XmlElementAttribute("Comp", typeof(BodyComp))]
        [System.Xml.Serialization.XmlElementAttribute("CompCenter", typeof(BodyCompCenter))]
        [System.Xml.Serialization.XmlElementAttribute("CompHistory", typeof(BodyCompHistory))]
        [System.Xml.Serialization.XmlElementAttribute("CompIssuance", typeof(BodyCompIssuance))]
        [System.Xml.Serialization.XmlElementAttribute("CompList", typeof(BodyCompList))]
        [System.Xml.Serialization.XmlElementAttribute("CompListMessage", typeof(BodyCompListMessage))]
        [System.Xml.Serialization.XmlElementAttribute("CompListType", typeof(BodyCompListType))]
        [System.Xml.Serialization.XmlElementAttribute("CompRedemption", typeof(BodyCompRedemption))]
        [System.Xml.Serialization.XmlElementAttribute("CountryInfo", typeof(BodyCountryInfo))]
        [System.Xml.Serialization.XmlElementAttribute("CouponList", typeof(BodyCouponList))]
        [System.Xml.Serialization.XmlElementAttribute("CouponListPlayer", typeof(BodyCouponListPlayer))]
        [System.Xml.Serialization.XmlElementAttribute("CouponRedemption", typeof(BodyCouponRedemption))]
        [System.Xml.Serialization.XmlElementAttribute("CreditConditions", typeof(BodyCreditConditions))]
        [System.Xml.Serialization.XmlElementAttribute("CreditInfo", typeof(BodyCreditInfo))]
        [System.Xml.Serialization.XmlElementAttribute("EmailType", typeof(BodyEmailType))]
        [System.Xml.Serialization.XmlElementAttribute("Error", typeof(BodyError))]
        [System.Xml.Serialization.XmlElementAttribute("GamingProfiles", typeof(BodyGamingProfiles))]
        [System.Xml.Serialization.XmlElementAttribute("Itinerary", typeof(BodyItinerary))]
        [System.Xml.Serialization.XmlElementAttribute("Language", typeof(BodyLanguage))]
        [System.Xml.Serialization.XmlElementAttribute("LinkInfo", typeof(BodyLinkInfo))]
        [System.Xml.Serialization.XmlElementAttribute("LoginModify", typeof(BodyLoginModify))]
        [System.Xml.Serialization.XmlElementAttribute("Merge", typeof(BodyMerge))]
        [System.Xml.Serialization.XmlElementAttribute("Messages", typeof(BodyMessages))]
        [System.Xml.Serialization.XmlElementAttribute("PINInfo", typeof(BodyPINInfo))]
        [System.Xml.Serialization.XmlElementAttribute("PhoneType", typeof(BodyPhoneType))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerActivity", typeof(BodyPlayerActivity))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerBalanceAdjustment", typeof(BodyPlayerBalanceAdjustment))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerBalances", typeof(BodyPlayerBalances))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerCard", typeof(BodyPlayerCard))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerCompListMessage", typeof(BodyPlayerCompListMessage))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerFind", typeof(BodyPlayerFind))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerFind2", typeof(BodyPlayerFind2))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerGroupAdd", typeof(BodyPlayerGroupAdd))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerGroupLookup", typeof(BodyPlayerGroupLookup))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerHost", typeof(BodyPlayerHost))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerImage", typeof(BodyPlayerImage))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerInterests", typeof(BodyPlayerInterests))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerPIN", typeof(BodyPlayerPIN))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerProfile", typeof(BodyPlayerProfile))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerPromoEvent", typeof(BodyPlayerPromoEvent))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerPromos", typeof(BodyPlayerPromos))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerPromotions", typeof(BodyPlayerPromotions))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerRanking", typeof(BodyPlayerRanking))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerReservation", typeof(BodyPlayerReservation))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerRestrictions", typeof(BodyPlayerRestrictions))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerStatus", typeof(BodyPlayerStatus))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerXtraCreditBalances", typeof(BodyPlayerXtraCreditBalances))]
        [System.Xml.Serialization.XmlElementAttribute("PointsConversionRate", typeof(BodyPointsConversionRate))]
        [System.Xml.Serialization.XmlElementAttribute("Promo", typeof(BodyPromo))]
        [System.Xml.Serialization.XmlElementAttribute("PromoAwardPlayerBalance", typeof(BodyPromoAwardPlayerBalance))]
        [System.Xml.Serialization.XmlElementAttribute("PromoEvent", typeof(BodyPromoEvent))]
        [System.Xml.Serialization.XmlElementAttribute("PromoEventBlock", typeof(BodyPromoEventBlock))]
        [System.Xml.Serialization.XmlElementAttribute("PromoMaster", typeof(BodyPromoMaster))]
        [System.Xml.Serialization.XmlElementAttribute("PromoStatus", typeof(BodyPromoStatus))]
        [System.Xml.Serialization.XmlElementAttribute("PromoType", typeof(BodyPromoType))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionAwardsIssuance", typeof(BodyPromotionAwardsIssuance))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionAwardsList", typeof(BodyPromotionAwardsList))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionAwardsListPlayer", typeof(BodyPromotionAwardsListPlayer))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionListPlayer", typeof(BodyPromotionListPlayer))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionListSystem", typeof(BodyPromotionListSystem))]
        [System.Xml.Serialization.XmlElementAttribute("PromotionPlayerStatusUpdate", typeof(BodyPromotionPlayerStatusUpdate))]
        [System.Xml.Serialization.XmlElementAttribute("RateRestriction", typeof(BodyRateRestriction))]
        [System.Xml.Serialization.XmlElementAttribute("RateType", typeof(BodyRateType))]
        [System.Xml.Serialization.XmlElementAttribute("Reason", typeof(BodyReason))]
        [System.Xml.Serialization.XmlElementAttribute("ReservationType", typeof(BodyReservationType))]
        [System.Xml.Serialization.XmlElementAttribute("Restrictions", typeof(BodyRestrictions))]
        [System.Xml.Serialization.XmlElementAttribute("RoomRestriction", typeof(BodyRoomRestriction))]
        [System.Xml.Serialization.XmlElementAttribute("RoomType", typeof(BodyRoomType))]
        [System.Xml.Serialization.XmlElementAttribute("SiteInfo", typeof(BodySiteInfo))]
        [System.Xml.Serialization.XmlElementAttribute("StopCodes", typeof(BodyStopCodes))]
        [System.Xml.Serialization.XmlElementAttribute("TripComment", typeof(BodyTripComment))]
        [System.Xml.Serialization.XmlElementAttribute("TripInformation", typeof(BodyTripInformation))]
        [System.Xml.Serialization.XmlElementAttribute("TripSummary", typeof(BodyTripSummary))]
        [System.Xml.Serialization.XmlElementAttribute("UnMerge", typeof(BodyUnMerge))]
        [System.Xml.Serialization.XmlElementAttribute("ZipCode", typeof(BodyZipCode))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyAbsUser
    {
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        public string LoginName;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string LastName;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string MiddleInitial;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string License;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string DisplayName;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string JobTitle;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> StaffID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StaffIDSpecified;
        
        public BodyAbsUser()
        {
            this.SiteID = 1;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyAddressType
    {
        
        /// <remarks/>
        public int TypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        /// <remarks/>
        public int Type;
        
        public BodyAddressType()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyAffiliation
    {
        
        /// <remarks/>
        public int AffiliationID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        public BodyAffiliation()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCardPrint
    {
        
        /// <remarks/>
        public int CardPrintRequestType;
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        public string Location;
        
        /// <remarks/>
        public string Workstation;
        
        /// <remarks/>
        public string Embosser;
        
        /// <remarks/>
        public string SerialNumber;
        
        /// <remarks/>
        public int RankingID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RankingIDSpecified;
        
        /// <remarks/>
        public string RankingName;
        
        /// <remarks/>
        public string CardID;
        
        /// <remarks/>
        public string FormattedCardID;
        
        /// <remarks/>
        public Name PlayerName;
        
        /// <remarks/>
        public string CardStatus;
        
        /// <remarks/>
        public System.DateTime ActivateDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActivateDateSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyComments
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comment", typeof(BodyCommentsComment))]
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyCommentsFilter))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCommentsComment
    {
        
        /// <remarks/>
        public bool IsHighPriority;
        
        /// <remarks/>
        public int Number;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberSpecified;
        
        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IsPrivate;
        
        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IsGlobal;
        
        /// <remarks/>
        public string Text;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public BodyCommentsCommentEnteredBy EnteredBy;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CommentGroupID", IsNullable=false)]
        public int[] CommentGroup;
        
        public BodyCommentsComment()
        {
            this.IsHighPriority = false;
            this.IsPrivate = false;
            this.IsGlobal = false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCommentsCommentEnteredBy
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCommentsFilter
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Site", IsNullable=false)]
        public SiteType[] Sites;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CommentGroupID", IsNullable=false)]
        public int[] CommentGroup;
        
        /// <remarks/>
        public System.DateTime CreatedBefore;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedBeforeSpecified;
        
        /// <remarks/>
        public System.DateTime CreatedAfter;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedAfterSpecified;
        
        /// <remarks/>
        public bool HighPriority;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HighPrioritySpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyComp
    {
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified;
        
        /// <remarks/>
        public string PrintedComments;
        
        /// <remarks/>
        public string RestrictedComments;
        
        /// <remarks/>
        public long RedemptionNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RedemptionNumberSpecified;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public System.DateTime IssuedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssuedDateSpecified;
        
        /// <remarks/>
        public System.DateTime EffectiveDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified;
        
        /// <remarks/>
        public decimal DollarValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DollarValueSpecified;
        
        /// <remarks/>
        public BodyCompItemNumber ItemNumber;
        
        /// <remarks/>
        public BodyCompCurrentUser CurrentUser;
        
        /// <remarks/>
        public BodyCompAuthorizer Authorizer;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public int VoidReasonID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VoidReasonIDSpecified;
        
        /// <remarks/>
        public string VoidDescription;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute(BodyCompSource.CompDollars)]
        public BodyCompSource Source;
        
        public BodyComp()
        {
            this.Source = BodyCompSource.CompDollars;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompItemNumber
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompCurrentUser
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompAuthorizer
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyCompSource
    {
        
        /// <remarks/>
        CompDollars,
        
        /// <remarks/>
        Points,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompCenter
    {
        
        /// <remarks/>
        public int CenterID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> ItemNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ItemNumberSpecified;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public int SiteID;
        
        public BodyCompCenter()
        {
            this.SiteID = 1;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompHistory
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comps", typeof(BodyCompHistoryComps))]
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyCompHistoryFilter))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompHistoryComps
    {
        
        /// <remarks/>
        public long RedemptionNumber;
        
        /// <remarks/>
        public BodyCompHistoryCompsStatus Status;
        
        /// <remarks/>
        public System.DateTime IssuedDate;
        
        /// <remarks/>
        public System.DateTime AccountingDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AccountingDateSpecified;
        
        /// <remarks/>
        public System.DateTime EffectiveDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified;
        
        /// <remarks/>
        public decimal DollarValue;
        
        /// <remarks/>
        public BodyCompHistoryCompsItemNumber ItemNumber;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompHistoryCompsStatus
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompHistoryCompsItemNumber
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompHistoryFilter
    {
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public int MaxNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxNumberSpecified;
        
        /// <remarks/>
        public int CompID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompIDSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompIssuance
    {
        
        /// <remarks/>
        public int CompID;
        
        /// <remarks/>
        public string CompDescription;
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        public decimal CompAmount;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompAmountSpecified;
        
        /// <remarks/>
        public string CompSource;
        
        /// <remarks/>
        public string FreeCompType;
        
        /// <remarks/>
        public long IssuanceID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssuanceIDSpecified;
        
        /// <remarks/>
        public string CompStatus;
        
        /// <remarks/>
        public string IssuerLogin;
        
        /// <remarks/>
        public string AuthorizerLogin;
        
        /// <remarks/>
        public System.DateTime IssuanceDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssuanceDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime EffectiveDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified;
        
        /// <remarks/>
        public string Receipt;
        
        /// <remarks/>
        public string PrintedComment;
        
        /// <remarks/>
        public string RestrictedComment;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompList
    {
        
        /// <remarks/>
        public int ItemNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> TypeID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TypeIDSpecified;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<decimal> DefaultDollarValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DefaultDollarValueSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> PercentToCharge;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PercentToChargeSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> DaysValid;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaysValidSpecified;
        
        /// <remarks/>
        public string PrintReceipt;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public int PrinterType;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> OptionalPrinter;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OptionalPrinterSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string OptionalForm;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> DeptID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeptIDSpecified;
        
        /// <remarks/>
        public int CheckID;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        /// <remarks/>
        public string AutoRedeem;
        
        /// <remarks/>
        public int SystemVisibility;
        
        public BodyCompList()
        {
            this.PrintReceipt = "N";
            this.SiteID = 1;
            this.PrinterType = 0;
            this.CheckID = 1;
            this.Status = "A";
            this.AutoRedeem = "N";
            this.SystemVisibility = 1;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompListMessage
    {
        
        /// <remarks/>
        public long CompID;
        
        /// <remarks/>
        public string CompType;
        
        /// <remarks/>
        public string CompDescription;
        
        /// <remarks/>
        public string Department;
        
        /// <remarks/>
        public string RestrictedBy;
        
        /// <remarks/>
        public string System;
        
        /// <remarks/>
        public string AutoRedeem;
        
        /// <remarks/>
        public string PrintReceipt;
        
        /// <remarks/>
        public string ExternalReference;
        
        /// <remarks/>
        public int DaysValid;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaysValidSpecified;
        
        /// <remarks/>
        public decimal CompRetailValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompRetailValueSpecified;
        
        /// <remarks/>
        public int ChargePercentage;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChargePercentageSpecified;
        
        /// <remarks/>
        public int PointsValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointsValueSpecified;
        
        /// <remarks/>
        public decimal CompValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompValueSpecified;
        
        /// <remarks/>
        public int CRBValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CRBValueSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompListType
    {
        
        /// <remarks/>
        public int TypeID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Description;
        
        /// <remarks/>
        public string Expense;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> SubTypeOf;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SubTypeOfSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Code;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public string AllowPastDateIssue;
        
        public BodyCompListType()
        {
            this.Status = "A";
            this.AllowPastDateIssue = "N";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompRedemption
    {
        
        /// <remarks/>
        public long IssuanceID;
        
        /// <remarks/>
        public int CompID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompIDSpecified;
        
        /// <remarks/>
        public string CompDescription;
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified;
        
        /// <remarks/>
        public decimal CompAmount;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompAmountSpecified;
        
        /// <remarks/>
        public string UserLogin;
        
        /// <remarks/>
        public string RedemptionCenter;
        
        /// <remarks/>
        public string CompStatus;
        
        /// <remarks/>
        public System.DateTime RedeemedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RedeemedDateSpecified;
        
        /// <remarks/>
        public BodyCompRedemptionItemizers Itemizers;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompRedemptionItemizers
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Item")]
        public BodyCompRedemptionItemizersItem[] Item;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCompRedemptionItemizersItem
    {
        
        /// <remarks/>
        public int Ordinal;
        
        /// <remarks/>
        public decimal Amount;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCountryInfo
    {
        
        /// <remarks/>
        public int CountryID;
        
        /// <remarks/>
        public string CountryDescription;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ZipFormat;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string GovtIDFormat;
        
        /// <remarks/>
        public string Abrv2;
        
        /// <remarks/>
        public string Abrv3;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string PhoneFormat;
        
        /// <remarks/>
        public string Validate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> SegmentID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SegmentIDSpecified;
        
        public BodyCountryInfo()
        {
            this.Abrv2 = "XX";
            this.Abrv3 = "XXX";
            this.Validate = "Y";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponList
    {
        
        /// <remarks/>
        public int CouponID;
        
        /// <remarks/>
        public string CouponDescription;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public string CreatedBy;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime ExpirationDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationDateSpecified;
        
        /// <remarks/>
        public string CouponType;
        
        /// <remarks/>
        public int NumberOffered;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberOfferedSpecified;
        
        /// <remarks/>
        public int MaxAllowedPerDay;
        
        /// <remarks/>
        public int MaxAllowedTotal;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxAllowedTotalSpecified;
        
        /// <remarks/>
        public string CouponValueType;
        
        /// <remarks/>
        public string LocalOrGlobal;
        
        /// <remarks/>
        public string CouponItem;
        
        /// <remarks/>
        public decimal CouponValue;
        
        /// <remarks/>
        public int DaysValid;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaysValidSpecified;
        
        /// <remarks/>
        public string Tiered;
        
        /// <remarks/>
        public string TierType;
        
        /// <remarks/>
        public object TierTracking;
        
        /// <remarks/>
        public string Qualify;
        
        /// <remarks/>
        public int QualifyValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QualifyValueSpecified;
        
        /// <remarks/>
        public string QualifyType;
        
        /// <remarks/>
        public string QualifyIn;
        
        /// <remarks/>
        public string TrackedBy;
        
        /// <remarks/>
        public int TrackedPeriod;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TrackedPeriodSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EarnSite")]
        public BodyCouponListEarnSite[] EarnSite;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RedeemSite")]
        public BodyCouponListRedeemSite[] RedeemSite;
        
        /// <remarks/>
        public BodyCouponListAvailability Availability;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListEarnSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListRedeemSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListAvailability
    {
        
        /// <remarks/>
        public string AvailabilityRestricted;
        
        /// <remarks/>
        public string AvailabilityStart;
        
        /// <remarks/>
        public string AvailabilityEnd;
        
        /// <remarks/>
        public string AvailabilityRecurrencePattern;
        
        /// <remarks/>
        public int AvailabilityMonthlyDay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityMonthlyDaySpecified;
        
        /// <remarks/>
        public string AvailabilityMonthlyFrequency;
        
        /// <remarks/>
        public string AvailabilityMonthlyFrequencyDay;
        
        /// <remarks/>
        public string AvailabilityYearlyMonth;
        
        /// <remarks/>
        public int AvailabilityYearlyDay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityYearlyDaySpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime AvailabilityRangeStart;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityRangeStartSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime AvailabilityRangeEnd;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityRangeEndSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AvailabilityExceptionDate", DataType="date")]
        public System.DateTime[] AvailabilityExceptionDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AvailabilityWeeklyDay")]
        public string[] AvailabilityWeeklyDay;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListPlayer
    {
        
        /// <remarks/>
        public int CouponID;
        
        /// <remarks/>
        public string CouponDescription;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public string CreatedBy;
        
        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public string StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime ExpirationDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationDateSpecified;
        
        /// <remarks/>
        public string CouponType;
        
        /// <remarks/>
        public string CouponValueType;
        
        /// <remarks/>
        public string LocalOrGlobal;
        
        /// <remarks/>
        public string CouponItem;
        
        /// <remarks/>
        public decimal CouponValue;
        
        /// <remarks/>
        public int DaysValid;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaysValidSpecified;
        
        /// <remarks/>
        public string Tiered;
        
        /// <remarks/>
        public string TierType;
        
        /// <remarks/>
        public object TierTracking;
        
        /// <remarks/>
        public string Qualify;
        
        /// <remarks/>
        public int QualifyValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QualifyValueSpecified;
        
        /// <remarks/>
        public string QualifyType;
        
        /// <remarks/>
        public string QualifyIn;
        
        /// <remarks/>
        public string TrackedBy;
        
        /// <remarks/>
        public int TrackedPeriod;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TrackedPeriodSpecified;
        
        /// <remarks/>
        public string CouponStatus;
        
        /// <remarks/>
        public string RedeemedBy;
        
        /// <remarks/>
        public System.DateTime RedeemedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RedeemedDateSpecified;
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EarnSite")]
        public BodyCouponListPlayerEarnSite[] EarnSite;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RedeemSite")]
        public BodyCouponListPlayerRedeemSite[] RedeemSite;
        
        /// <remarks/>
        public BodyCouponListPlayerAvailability Availability;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListPlayerEarnSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListPlayerRedeemSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponListPlayerAvailability
    {
        
        /// <remarks/>
        public string AvailabilityRestricted;
        
        /// <remarks/>
        public string AvailabilityStart;
        
        /// <remarks/>
        public string AvailabilityEnd;
        
        /// <remarks/>
        public string AvailabilityRecurrencePattern;
        
        /// <remarks/>
        public int AvailabilityMonthlyDay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityMonthlyDaySpecified;
        
        /// <remarks/>
        public string AvailabilityMonthlyFrequency;
        
        /// <remarks/>
        public string AvailabilityMonthlyFrequencyDay;
        
        /// <remarks/>
        public string AvailabilityYearlyMonth;
        
        /// <remarks/>
        public int AvailabilityYearlyDay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityYearlyDaySpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime AvailabilityRangeStart;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityRangeStartSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime AvailabilityRangeEnd;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailabilityRangeEndSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AvailabilityWeeklyDay")]
        public string[] AvailabilityWeeklyDay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AvailabilityExceptionDate", DataType="date")]
        public System.DateTime[] AvailabilityExceptionDate;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCouponRedemption
    {
        
        /// <remarks/>
        public int CouponID;
        
        /// <remarks/>
        public string CouponDescription;
        
        /// <remarks/>
        public string UserLogin;
        
        /// <remarks/>
        public int CouponQuantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CouponQuantitySpecified;
        
        /// <remarks/>
        public decimal CouponAmount;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CouponAmountSpecified;
        
        /// <remarks/>
        public string CouponItem;
        
        /// <remarks/>
        public string CouponStatus;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditConditions
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyCreditConditionsFilter))]
        [System.Xml.Serialization.XmlElementAttribute("PropertyCreditConditions", typeof(BodyCreditConditionsPropertyCreditConditions))]
        public object Item;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditConditionsFilter
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Site", IsNullable=false)]
        public SiteType[] Sites;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditConditionsPropertyCreditConditions
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Condition")]
        public BodyCreditConditionsPropertyCreditConditionsCondition[] Condition;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditConditionsPropertyCreditConditionsCondition
    {
        
        /// <remarks/>
        public string CreditConditionID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Priority;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditInfo
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CreditAccount")]
        public BodyCreditInfoCreditAccount[] CreditAccount;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyCreditInfoCreditAccount
    {
        
        /// <remarks/>
        public decimal CreditLine;
        
        /// <remarks/>
        public decimal FrontMoney;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FrontMoneySpecified;
        
        /// <remarks/>
        public decimal Used;
        
        /// <remarks/>
        public decimal InTransit;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool InTransitSpecified;
        
        /// <remarks/>
        public decimal Available;
        
        /// <remarks/>
        public System.DateTime LastMarkerDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastMarkerDateSpecified;
        
        /// <remarks/>
        public string CreditStopCodes;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Account;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AccountSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool TTO;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TTOSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyEmailType
    {
        
        /// <remarks/>
        public int TypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        public BodyEmailType()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyError
    {
        
        /// <remarks/>
        public string ErrorCode;
        
        /// <remarks/>
        public string ErrorDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyGamingProfiles
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GamingProfile")]
        public BodyGamingProfilesGamingProfile[] GamingProfile;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyGamingProfilesGamingProfile
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CompStatus;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string FavoriteGame;
        
        /// <remarks/>
        public decimal CompBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompBalanceSpecified;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public BodyGamingProfilesGamingProfileAccountStatus AccountStatus;
        
        /// <remarks/>
        public BodyGamingProfilesGamingProfileHost Host;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public BodyGamingProfilesGamingProfileRanking Ranking;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyGamingProfilesGamingProfileAccountStatus
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyGamingProfilesGamingProfileHost
    {
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        public string LoginName;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string License;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyGamingProfilesGamingProfileRanking
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyItinerary
    {
        
        /// <remarks/>
        public string ReservationID;
        
        /// <remarks/>
        public string ItineraryStartDate;
        
        /// <remarks/>
        public string ItineraryEndDate;
        
        /// <remarks/>
        public string CRMGUID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RoomReservation")]
        public RoomReservation[] RoomReservation;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Promotion")]
        public BodyItineraryPromotion[] Promotion;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TripComment")]
        public BodyItineraryTripComment[] TripComment;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyItineraryPromotion
    {
        
        /// <remarks/>
        public string Action;
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        public PromoStatus PromoStatus;
        
        /// <remarks/>
        public string CRMGUID;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyItineraryTripComment
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> ReservationID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReservationIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ReservationXRef;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> CommentID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CommentIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CommentXRef;
        
        /// <remarks/>
        public object Confirmation;
        
        /// <remarks/>
        public BodyItineraryTripCommentAction Action;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> Date;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Comment;
        
        /// <remarks/>
        public User User;
        
        /// <remarks/>
        public object CRMGUID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyItineraryTripCommentAction
    {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Update,
        
        /// <remarks/>
        Cancel,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyLanguage
    {
        
        /// <remarks/>
        public int LanguageID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        public BodyLanguage()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyLinkInfo
    {
        
        /// <remarks/>
        public object LinkID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("LinkedPlayer", IsNullable=false)]
        public BodyLinkInfoLinkedPlayer[] LinkedPlayers;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyLinkInfoLinkedPlayer
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public decimal CompBalance;
        
        /// <remarks/>
        public long PointBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointBalanceSpecified;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyLoginModify
    {
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public string LoginName;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyMerge
    {
        
        /// <remarks/>
        public int VictimPlayerID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyMessages
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("HostMessage", typeof(BodyMessagesHostMessage))]
        [System.Xml.Serialization.XmlElementAttribute("Message", typeof(BodyMessagesMessage))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyMessagesHostMessage
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public int MessageID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MessageIDSpecified;
        
        /// <remarks/>
        public string Action;
        
        /// <remarks/>
        public string Subject;
        
        /// <remarks/>
        public System.DateTime Date;
        
        /// <remarks/>
        public string MessageBody;
        
        /// <remarks/>
        public string Command;
        
        /// <remarks/>
        public int Count;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CountSpecified;
        
        /// <remarks/>
        public string Orderby;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyMessagesMessage
    {
        
        /// <remarks/>
        public int ContactID;
        
        /// <remarks/>
        public int FollowUpID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FollowUpIDSpecified;
        
        /// <remarks/>
        public string Subject;
        
        /// <remarks/>
        public System.DateTime Date;
        
        /// <remarks/>
        public string MessageBody;
        
        /// <remarks/>
        public string Command;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPINInfo
    {
        
        /// <remarks/>
        public string PINDigest;
        
        /// <remarks/>
        public int Sentinel;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SentinelSpecified;
        
        /// <remarks/>
        public int EncryptIndex;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EncryptIndexSpecified;
        
        /// <remarks/>
        public string Seed;
        
        /// <remarks/>
        public bool PINLock;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PINLockSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPhoneType
    {
        
        /// <remarks/>
        public int TypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        /// <remarks/>
        public int Type;
        
        public BodyPhoneType()
        {
            this.Status = "A";
            this.Type = 3;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerActivity
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime EndDate;
        
        /// <remarks/>
        public int SlotPointsEarned;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SlotPointsEarnedSpecified;
        
        /// <remarks/>
        public int TablePointsEarned;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TablePointsEarnedSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerBalanceAdjustment
    {
        
        /// <remarks/>
        public BodyPlayerBalanceAdjustmentBalanceType BalanceType;
        
        /// <remarks/>
        public string CreditOrDebit;
        
        /// <remarks/>
        public decimal Amount;
        
        /// <remarks/>
        public string RewardProgram;
        
        /// <remarks/>
        public string LocalOrGlobal;
        
        /// <remarks/>
        public System.DateTime AvailableDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailableDateSpecified;
        
        /// <remarks/>
        public System.DateTime ExpirationDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationDateSpecified;
        
        /// <remarks/>
        public decimal OldCompBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldCompBalanceSpecified;
        
        /// <remarks/>
        public decimal NewCompBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NewCompBalanceSpecified;
        
        /// <remarks/>
        public decimal OldXtraCreditBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldXtraCreditBalanceSpecified;
        
        /// <remarks/>
        public decimal NewXtraCreditBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NewXtraCreditBalanceSpecified;
        
        /// <remarks/>
        public long OldPointBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldPointBalanceSpecified;
        
        /// <remarks/>
        public long NewPointBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NewPointBalanceSpecified;
        
        /// <remarks/>
        public long OldRewardBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldRewardBalanceSpecified;
        
        /// <remarks/>
        public long NewRewardBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NewRewardBalanceSpecified;
        
        /// <remarks/>
        public string UserLogin;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerBalanceAdjustmentBalanceType
    {
        
        /// <remarks/>
        Comp,
        
        /// <remarks/>
        Points,
        
        /// <remarks/>
        Reward,
        
        /// <remarks/>
        XtraCredit,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerBalances
    {
        
        /// <remarks/>
        public int RewardBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RewardBalanceSpecified;
        
        /// <remarks/>
        public decimal CompBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompBalanceSpecified;
        
        /// <remarks/>
        public decimal GiftPoints;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GiftPointsSpecified;
        
        /// <remarks/>
        public decimal XtraCreditBalanceLocal;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool XtraCreditBalanceLocalSpecified;
        
        /// <remarks/>
        public decimal XtraCreditBalanceGlobal;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool XtraCreditBalanceGlobalSpecified;
        
        /// <remarks/>
        public long PointBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointBalanceSpecified;
        
        /// <remarks/>
        public decimal CompBalanceLinked;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompBalanceLinkedSpecified;
        
        /// <remarks/>
        public decimal GiftPointsLinked;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GiftPointsLinkedSpecified;
        
        /// <remarks/>
        public long PointBalanceLinked;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointBalanceLinkedSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerCard
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CardNumber", IsNullable=false)]
        public CardNumbersCardNumber[] CardNumbers;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class CardNumbersCardNumber
    {
        
        /// <remarks/>
        public string CardID;
        
        /// <remarks/>
        public string Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerCompListMessage
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comp")]
        public BodyPlayerCompListMessageComp[] Comp;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerCompListMessageComp
    {
        
        /// <remarks/>
        public int CompID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompIDSpecified;
        
        /// <remarks/>
        public int IssuanceID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssuanceIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime IssuanceDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssuanceDateSpecified;
        
        /// <remarks/>
        public string CompType;
        
        /// <remarks/>
        public string CompDescription;
        
        /// <remarks/>
        public string CompSource;
        
        /// <remarks/>
        public string FreeCompType;
        
        /// <remarks/>
        public string RestrictedBy;
        
        /// <remarks/>
        public string System;
        
        /// <remarks/>
        public string CompStatus;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime LastStatusChangeDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastStatusChangeDateSpecified;
        
        /// <remarks/>
        public int DaysValid;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaysValidSpecified;
        
        /// <remarks/>
        public decimal CompRetailValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompRetailValueSpecified;
        
        /// <remarks/>
        public int ChargePercentage;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChargePercentageSpecified;
        
        /// <remarks/>
        public int PointValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointValueSpecified;
        
        /// <remarks/>
        public decimal CompValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompValueSpecified;
        
        /// <remarks/>
        public int CRBValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CRBValueSpecified;
        
        /// <remarks/>
        public int Quantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified;
        
        /// <remarks/>
        public int TripNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TripNumberSpecified;
        
        /// <remarks/>
        public string PostedBy;
        
        /// <remarks/>
        public string VoidedBy;
        
        /// <remarks/>
        public string VoidReason;
        
        /// <remarks/>
        public string IssuerLogin;
        
        /// <remarks/>
        public string AuthorizerLogin;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFind
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyPlayerFindFilter))]
        [System.Xml.Serialization.XmlElementAttribute("PlayersFound", typeof(BodyPlayerFindPlayersFound))]
        public object Item;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilter
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Search", typeof(BodyPlayerFindFilterSearch))]
        [System.Xml.Serialization.XmlElementAttribute("SearchCardID", typeof(BodyPlayerFindFilterSearchCardID))]
        [System.Xml.Serialization.XmlElementAttribute("SearchName", typeof(BodyPlayerFindFilterSearchName))]
        [System.Xml.Serialization.XmlElementAttribute("SearchPlayerID", typeof(BodyPlayerFindFilterSearchPlayerID))]
        [System.Xml.Serialization.XmlElementAttribute("SearchSSN", typeof(BodyPlayerFindFilterSearchSSN))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilterSearch
    {
        
        /// <remarks/>
        public string License;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime DateOfBirth;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateOfBirthSpecified;
        
        /// <remarks/>
        public string PhoneNumber;
        
        /// <remarks/>
        public string PostalCode;
        
        /// <remarks/>
        public BodyPlayerFindFilterSearchStatus Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerFindFilterSearchStatus
    {
        
        /// <remarks/>
        All,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("In House")]
        InHouse,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Checked Out")]
        CheckedOut,
        
        /// <remarks/>
        Cancelled,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("No Show")]
        NoShow,
        
        /// <remarks/>
        Pending,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilterSearchCardID
    {
        
        /// <remarks/>
        public string CardID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilterSearchName
    {
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string LastName;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilterSearchPlayerID
    {
        
        /// <remarks/>
        public int PlayerID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindFilterSearchSSN
    {
        
        /// <remarks/>
        public string SSN;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindPlayersFound
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PlayerFound")]
        public BodyPlayerFindPlayersFoundPlayerFound[] PlayerFound;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFindPlayersFoundPlayerFound
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string MiddleName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string PreferredName;
        
        /// <remarks/>
        public string MailingCity;
        
        /// <remarks/>
        public string MailingState;
        
        /// <remarks/>
        public string MailingCountry;
        
        /// <remarks/>
        public string Ranking;
        
        /// <remarks/>
        public string SSN;
        
        /// <remarks/>
        public string DateOfBirth;
        
        /// <remarks/>
        public bool CreditAccount;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreditAccountSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFind2
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyPlayerFind2Filter))]
        [System.Xml.Serialization.XmlElementAttribute("PlayersFound", typeof(BodyPlayerFind2PlayersFound))]
        public object Item;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFind2Filter
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CardID", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("DateOfBirth", typeof(System.DateTime), DataType="date")]
        [System.Xml.Serialization.XmlElementAttribute("FirstName", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("LastName", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("License", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("PhoneNumber", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerID", typeof(int))]
        [System.Xml.Serialization.XmlElementAttribute("PostalCode", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("SSN", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("Status", typeof(BodyPlayerFind2FilterStatus))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
        public object[] Items;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemsChoiceType[] ItemsElementName;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerFind2FilterStatus
    {
        
        /// <remarks/>
        All,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("In House")]
        InHouse,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Checked Out")]
        CheckedOut,
        
        /// <remarks/>
        Cancelled,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("No Show")]
        NoShow,
        
        /// <remarks/>
        Pending,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
    public enum ItemsChoiceType
    {
        
        /// <remarks/>
        CardID,
        
        /// <remarks/>
        DateOfBirth,
        
        /// <remarks/>
        FirstName,
        
        /// <remarks/>
        LastName,
        
        /// <remarks/>
        License,
        
        /// <remarks/>
        PhoneNumber,
        
        /// <remarks/>
        PlayerID,
        
        /// <remarks/>
        PostalCode,
        
        /// <remarks/>
        SSN,
        
        /// <remarks/>
        Status,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFind2PlayersFound
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PlayerFound")]
        public BodyPlayerFind2PlayersFoundPlayerFound[] PlayerFound;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerFind2PlayersFoundPlayerFound
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        public object CardID;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string MiddleName;
        
        /// <remarks/>
        public string PreferredName;
        
        /// <remarks/>
        public string DateOfBirth;
        
        /// <remarks/>
        public string City;
        
        /// <remarks/>
        public string State;
        
        /// <remarks/>
        public string Country;
        
        /// <remarks/>
        public string CreditAccount;
        
        /// <remarks/>
        public string Ranking;
        
        /// <remarks/>
        public BodyPlayerFind2PlayersFoundPlayerFoundStatus Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerFind2PlayersFoundPlayerFoundStatus
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("In House")]
        InHouse,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Play Only")]
        PlayOnly,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Checked Out")]
        CheckedOut,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupAdd
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public string GroupName;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupLookup
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyPlayerGroupLookupFilter))]
        [System.Xml.Serialization.XmlElementAttribute("GroupInfo", typeof(BodyPlayerGroupLookupGroupInfo))]
        public object Item;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupLookupFilter
    {
        
        /// <remarks/>
        public string GroupName;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupLookupGroupInfo
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public SiteType Site;
        
        /// <remarks/>
        public BodyPlayerGroupLookupGroupInfoGroup Group;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Player", IsNullable=false)]
        public BodyPlayerGroupLookupGroupInfoPlayer[] PlayerGroup;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupLookupGroupInfoGroup
    {
        
        /// <remarks/>
        public string GroupID;
        
        /// <remarks/>
        public string GroupName;
        
        /// <remarks/>
        public string Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerGroupLookupGroupInfoPlayer
    {
        
        /// <remarks/>
        public string Location;
        
        /// <remarks/>
        public BodyPlayerGroupLookupGroupInfoPlayerInCasino InCasino;
        
        /// <remarks/>
        public string PlayerID;
        
        /// <remarks/>
        public Name Name;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerGroupLookupGroupInfoPlayerInCasino
    {
        
        /// <remarks/>
        TRUE,
        
        /// <remarks/>
        FALSE,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerHost
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public int HostID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerImage
    {
        
        /// <remarks/>
        public int ImageType;
        
        /// <remarks/>
        public string ImageSubType;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] Image;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerInterests
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PlayerInterest")]
        public BodyPlayerInterestsPlayerInterest[] PlayerInterest;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerInterestsPlayerInterest
    {
        
        /// <remarks/>
        public int InterestNumber;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPIN
    {
        
        /// <remarks/>
        public int PINNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PINNumberSpecified;
        
        /// <remarks/>
        public string PINDigest;
        
        /// <remarks/>
        public int OldPINNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldPINNumberSpecified;
        
        /// <remarks/>
        public string OldPINDigest;
        
        /// <remarks/>
        public int PINNumberVerify;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PINNumberVerifySpecified;
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UserIDSpecified;
        
        /// <remarks/>
        public string ValidateResult;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfile
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime DateofBirth;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateofBirthSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime Anniversary;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AnniversarySpecified;
        
        /// <remarks/>
        public bool CreditAccount;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreditAccountSpecified;
        
        /// <remarks/>
        public string Gender;
        
        /// <remarks/>
        public string WebEnabled;
        
        /// <remarks/>
        public string Exempt;
        
        /// <remarks/>
        public string Attraction;
        
        /// <remarks/>
        public string Affiliation;
        
        /// <remarks/>
        public string CrossReference;
        
        /// <remarks/>
        public string EnrollmentLocation;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime DateEnrolled;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateEnrolledSpecified;
        
        /// <remarks/>
        public Name Name;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Address", IsNullable=false)]
        public BodyPlayerProfileAddress[] Addresses;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PhoneNumber", IsNullable=false)]
        public BodyPlayerProfilePhoneNumber[] PhoneNumbers;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Identification", IsNullable=false)]
        public BodyPlayerProfileIdentification[] Identifications;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Email", IsNullable=false)]
        public BodyPlayerProfileEmail[] Emails;
        
        /// <remarks/>
        public BodyPlayerProfileLanguages Languages;
        
        /// <remarks/>
        public BodyPlayerProfileEnrolledBy EnrolledBy;
        
        /// <remarks/>
        public BodyPlayerProfileStatus Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CardNumber", IsNullable=false)]
        public CardNumbersCardNumber[] CardNumbers;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Restriction", IsNullable=false)]
        public BodyPlayerProfileRestriction[] PlayerRestrictions;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Interest", IsNullable=false)]
        public BodyPlayerProfileInterest[] Interests;
        
        /// <remarks/>
        public BodyPlayerProfileEmployment Employment;
        
        /// <remarks/>
        public BodyPlayerProfileFrequentFlyers FrequentFlyers;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SiteInfo", IsNullable=false)]
        public BodyPlayerProfileSiteInfo[] SiteParameters;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileAddress
    {
        
        /// <remarks/>
        public string Address1;
        
        /// <remarks/>
        public string Address2;
        
        /// <remarks/>
        public string City;
        
        /// <remarks/>
        public string StateProvince;
        
        /// <remarks/>
        public string PostalCode;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Country;
        
        /// <remarks/>
        public string Location;
        
        /// <remarks/>
        public bool Deliverable;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeliverableSpecified;
        
        /// <remarks/>
        public bool Preferred;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreferredSpecified;
        
        /// <remarks/>
        public string Suburb;
        
        /// <remarks/>
        public BodyPlayerProfileAddressCreditAddress CreditAddress;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreditAddressSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerProfileAddressCreditAddress
    {
        
        /// <remarks/>
        Y,
        
        /// <remarks/>
        N,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfilePhoneNumber
    {
        
        /// <remarks/>
        public string Number;
        
        /// <remarks/>
        public string Extension;
        
        /// <remarks/>
        public string Location;
        
        /// <remarks/>
        public bool Preferred;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreferredSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileIdentification
    {
        
        /// <remarks/>
        public string Type;
        
        /// <remarks/>
        public string IDNumber;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime ExpirationDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime VerificationDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VerificationDateSpecified;
        
        /// <remarks/>
        public string StateProvince;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Country;
        
        /// <remarks/>
        public BodyPlayerProfileIdentificationPrimary Primary;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimarySpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerProfileIdentificationPrimary
    {
        
        /// <remarks/>
        Y,
        
        /// <remarks/>
        N,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileEmail
    {
        
        /// <remarks/>
        public string Address;
        
        /// <remarks/>
        public string Location;
        
        /// <remarks/>
        public bool Preferred;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreferredSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime AddDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AddDateSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileLanguages
    {
        
        /// <remarks/>
        public string Language;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileEnrolledBy
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileStatus
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileRestriction
    {
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public System.DateTime Expiration;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileInterest
    {
        
        /// <remarks/>
        public int Code;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodeSpecified;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public BodyPlayerProfileInterestModifyInterest ModifyInterest;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ModifyInterestSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerProfileInterestModifyInterest
    {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Delete,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileEmployment
    {
        
        /// <remarks/>
        public string Company;
        
        /// <remarks/>
        public string Position;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileFrequentFlyers
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FrequentFlyer")]
        public BodyPlayerProfileFrequentFlyersFrequentFlyer[] FrequentFlyer;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileFrequentFlyersFrequentFlyer
    {
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Value;
        
        /// <remarks/>
        public BodyPlayerProfileFrequentFlyersFrequentFlyerPreferred Preferred;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreferredSpecified;
        
        /// <remarks/>
        public BodyPlayerProfileFrequentFlyersFrequentFlyerModifyFrequentFlyer ModifyFrequentFlyer;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ModifyFrequentFlyerSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerProfileFrequentFlyersFrequentFlyerPreferred
    {
        
        /// <remarks/>
        Y,
        
        /// <remarks/>
        N,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyPlayerProfileFrequentFlyersFrequentFlyerModifyFrequentFlyer
    {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Delete,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileSiteInfo
    {
        
        /// <remarks/>
        public string SiteID;
        
        /// <remarks/>
        public long PointBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointBalanceSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public BodyPlayerProfileSiteInfoRanking Ranking;
        
        /// <remarks/>
        public BodyPlayerProfileSiteInfoHost Host;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileSiteInfoRanking
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerProfileSiteInfoHost
    {
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        public string LoginName;
        
        /// <remarks/>
        public string FirstName;
        
        /// <remarks/>
        public string LastName;
        
        /// <remarks/>
        public string License;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromoEvent
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PlayerIDSpecified;
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        public int BlockID;
        
        /// <remarks/>
        public int Tickets;
        
        /// <remarks/>
        public string Received;
        
        /// <remarks/>
        public int OldBlockID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OldBlockIDSpecified;
        
        /// <remarks/>
        public string UserLogin;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromos
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PlayerPromoFilter", typeof(BodyPlayerPromosPlayerPromoFilter))]
        [System.Xml.Serialization.XmlElementAttribute("PlayerPromos", typeof(BodyPlayerPromosPlayerPromos))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromosPlayerPromoFilter
    {
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PromoIDSpecified;
        
        /// <remarks/>
        public int Count;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CountSpecified;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Site", IsNullable=false)]
        public SiteType[] Sites;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public string OrderBy;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromosPlayerPromos
    {
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        public string PromoName;
        
        /// <remarks/>
        public string PublicDescription;
        
        /// <remarks/>
        public string BeginDate;
        
        /// <remarks/>
        public string EndDate;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromotions
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Promotion")]
        public BodyPlayerPromotionsPromotion[] Promotion;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromotionsPromotion
    {
        
        /// <remarks/>
        public BodyPlayerPromotionsPromotionSite Site;
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        public BodyPlayerPromotionsPromotionPromoStatus PromoStatus;
        
        /// <remarks/>
        public string CRMGUID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromotionsPromotionSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteIDSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerPromotionsPromotionPromoStatus
    {
        
        /// <remarks/>
        public int Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerRanking
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public BodyPlayerRankingRanking Ranking;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerRankingRanking
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerReservation
    {
        
        /// <remarks/>
        public string ItineraryID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public string ReservationStatus;
        
        /// <remarks/>
        public BodyPlayerReservationTripAuthorizer TripAuthorizer;
        
        /// <remarks/>
        public BodyPlayerReservationSecondAuthorizer SecondAuthorizer;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public RoomReservation[] RoomReservations;
        
        /// <remarks/>
        public BodyPlayerReservationCurrentUser CurrentUser;
        
        /// <remarks/>
        public SiteType Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerReservationTripAuthorizer
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerReservationSecondAuthorizer
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerReservationCurrentUser
    {
        
        /// <remarks/>
        public User User;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerRestrictions
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PlayerRestriction")]
        public BodyPlayerRestrictionsPlayerRestriction[] PlayerRestriction;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerRestrictionsPlayerRestriction
    {
        
        /// <remarks/>
        public string RestrictionName;
        
        /// <remarks/>
        public string RestrictionType;
        
        /// <remarks/>
        public System.DateTime RestrictionExpiration;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RestrictionExpirationSpecified;
        
        /// <remarks/>
        public string RestrictionActions;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerStatus
    {
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public string Description;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerXtraCreditBalances
    {
        
        /// <remarks/>
        public decimal GlobalBalance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("LocalBalance", IsNullable=false)]
        public BodyPlayerXtraCreditBalancesLocalBalance[] LocalBalances;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPlayerXtraCreditBalancesLocalBalance
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteIDSpecified;
        
        /// <remarks/>
        public string SiteDescription;
        
        /// <remarks/>
        public decimal Balance;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BalanceSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPointsConversionRate
    {
        
        /// <remarks/>
        public decimal PointDollarValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointDollarValueSpecified;
        
        /// <remarks/>
        public decimal RewardDollarValue;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RewardDollarValueSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromo
    {
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        public string PromoName;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Description;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> MasterID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MasterIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string PublicDescription;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ImageURL;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string XRef;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        [System.ComponentModel.DefaultValueAttribute("N")]
        public string InternetDisplay;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string InternetDisplayText;
        
        public BodyPromo()
        {
            this.Status = "A";
            this.InternetDisplay = "N";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoAwardPlayerBalance
    {
        
        /// <remarks/>
        public int PromoAwardID;
        
        /// <remarks/>
        public int PromoID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PromoIDSpecified;
        
        /// <remarks/>
        public string PromotionName;
        
        /// <remarks/>
        public int Earned;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EarnedSpecified;
        
        /// <remarks/>
        public int Received;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReceivedSpecified;
        
        /// <remarks/>
        public int Available;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailableSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoEvent
    {
        
        /// <remarks/>
        public int EventID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public System.DateTime Date;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public int PromoID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoEventBlock
    {
        
        /// <remarks/>
        public int BlockID;
        
        /// <remarks/>
        public int EventID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int Tickets;
        
        /// <remarks/>
        public int MaxPerPlayer;
        
        /// <remarks/>
        public int DefaultAmount;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteIDSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoMaster
    {
        
        /// <remarks/>
        public int MasterID;
        
        /// <remarks/>
        public string MasterName;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Description;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public string XRef;
        
        public BodyPromoMaster()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoStatus
    {
        
        /// <remarks/>
        public int StatusNumber;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int Type;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TypeSpecified;
        
        /// <remarks/>
        public string Send;
        
        /// <remarks/>
        public string Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromoType
    {
        
        /// <remarks/>
        public int Type;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Multi;
        
        /// <remarks/>
        public string Status;
        
        public BodyPromoType()
        {
            this.Multi = "N";
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsIssuance
    {
        
        /// <remarks/>
        public int PromoAwardID;
        
        /// <remarks/>
        public string PromoAwardName;
        
        /// <remarks/>
        public int IssueQuantity;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssueQuantitySpecified;
        
        /// <remarks/>
        public string UserLogin;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Ticket")]
        public int[] Ticket;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsList
    {
        
        /// <remarks/>
        public int PromoAwardID;
        
        /// <remarks/>
        public string PromoAwardName;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public int Points;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointsSpecified;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public string CreatedBy;
        
        /// <remarks/>
        public string Hidden;
        
        /// <remarks/>
        public string Locked;
        
        /// <remarks/>
        public string DefaultReport;
        
        /// <remarks/>
        public string Report;
        
        /// <remarks/>
        public string PrintedComment;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EarnSite")]
        public BodyPromotionAwardsListEarnSite[] EarnSite;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RedeemSite")]
        public BodyPromotionAwardsListRedeemSite[] RedeemSite;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsListEarnSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsListRedeemSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsListPlayer
    {
        
        /// <remarks/>
        public int PromoAwardID;
        
        /// <remarks/>
        public string PromoAwardName;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public int Points;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PointsSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EarnSite")]
        public BodyPromotionAwardsListPlayerEarnSite[] EarnSite;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RedeemSite")]
        public BodyPromotionAwardsListPlayerRedeemSite[] RedeemSite;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsListPlayerEarnSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionAwardsListPlayerRedeemSite
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayer
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Promotion")]
        public BodyPromotionListPlayerPromotion[] Promotion;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotion
    {
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PlayerIDSpecified;
        
        /// <remarks/>
        public string PlayerCardID;
        
        /// <remarks/>
        public string PromotionStatus;
        
        /// <remarks/>
        public string PromotionName;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public string CreatedBy;
        
        /// <remarks/>
        public string Hidden;
        
        /// <remarks/>
        public string Locked;
        
        /// <remarks/>
        public string MasterPromotion;
        
        /// <remarks/>
        public string Criteria;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string PublicDescription;
        
        /// <remarks/>
        public string InternetDisplayEnabled;
        
        /// <remarks/>
        public string ImageURL;
        
        /// <remarks/>
        public string DisplayOnCalendar;
        
        /// <remarks/>
        public string AutoClubEnroll;
        
        /// <remarks/>
        public string AutoGamingEnroll;
        
        /// <remarks/>
        public BodyPromotionListPlayerPromotionLineItems LineItems;
        
        /// <remarks/>
        public BodyPromotionListPlayerPromotionPointMultiplier PointMultiplier;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionLineItems
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LineItemAssigned")]
        public BodyPromotionListPlayerPromotionLineItemsLineItemAssigned[] LineItemAssigned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionLineItemsLineItemAssigned
    {
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int BillTo;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BillToSpecified;
        
        /// <remarks/>
        public decimal Cost;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CostSpecified;
        
        /// <remarks/>
        public decimal Budget;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BudgetSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionPointMultiplier
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Multiplier")]
        public int[] Multiplier;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StartDate")]
        public System.DateTime[] StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EndDate")]
        public System.DateTime[] EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Sites")]
        public BodyPromotionListPlayerPromotionPointMultiplierSites[] Sites;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Availability")]
        public BodyPromotionListPlayerPromotionPointMultiplierAvailability[] Availability;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionPointMultiplierSites
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Site")]
        public BodyPromotionListPlayerPromotionPointMultiplierSitesSite[] Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionPointMultiplierSitesSite
    {
        
        /// <remarks/>
        public string SiteDescription;
        
        /// <remarks/>
        public string Used;
        
        /// <remarks/>
        public string Machines;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionPointMultiplierAvailability
    {
        
        /// <remarks/>
        public string RestrictedAvailability;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public BodyPromotionListPlayerPromotionPointMultiplierAvailabilityExceptionDates ExceptionDates;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListPlayerPromotionPointMultiplierAvailabilityExceptionDates
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Date")]
        public System.DateTime[] Date;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystem
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Promotion")]
        public BodyPromotionListSystemPromotion[] Promotion;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotion
    {
        
        /// <remarks/>
        public string PromotionName;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public System.DateTime CreatedDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedDateSpecified;
        
        /// <remarks/>
        public string CreatedBy;
        
        /// <remarks/>
        public string Hidden;
        
        /// <remarks/>
        public string Locked;
        
        /// <remarks/>
        public string MasterPromotion;
        
        /// <remarks/>
        public string Criteria;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string PublicDescription;
        
        /// <remarks/>
        public string InternetDisplayEnabled;
        
        /// <remarks/>
        public string ImageURL;
        
        /// <remarks/>
        public string DisplayOnCalendar;
        
        /// <remarks/>
        public string AutoClubEnroll;
        
        /// <remarks/>
        public string AutoGamingEnroll;
        
        /// <remarks/>
        public BodyPromotionListSystemPromotionLineItems LineItems;
        
        /// <remarks/>
        public BodyPromotionListSystemPromotionPointMultiplier PointMultiplier;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionLineItems
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LineItemAssigned")]
        public BodyPromotionListSystemPromotionLineItemsLineItemAssigned[] LineItemAssigned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionLineItemsLineItemAssigned
    {
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public int BillTo;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BillToSpecified;
        
        /// <remarks/>
        public decimal Cost;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CostSpecified;
        
        /// <remarks/>
        public decimal Budget;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BudgetSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionPointMultiplier
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Multiplier")]
        public int[] Multiplier;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StartDate")]
        public System.DateTime[] StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EndDate")]
        public System.DateTime[] EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Sites")]
        public BodyPromotionListSystemPromotionPointMultiplierSites[] Sites;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Availability")]
        public BodyPromotionListSystemPromotionPointMultiplierAvailability[] Availability;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionPointMultiplierSites
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Site")]
        public BodyPromotionListSystemPromotionPointMultiplierSitesSite[] Site;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionPointMultiplierSitesSite
    {
        
        /// <remarks/>
        public string SiteDescription;
        
        /// <remarks/>
        public string Used;
        
        /// <remarks/>
        public string Machines;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionPointMultiplierAvailability
    {
        
        /// <remarks/>
        public string RestrictedAvailability;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public BodyPromotionListSystemPromotionPointMultiplierAvailabilityExceptionDates ExceptionDates;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionListSystemPromotionPointMultiplierAvailabilityExceptionDates
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Date")]
        public System.DateTime[] Date;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyPromotionPlayerStatusUpdate
    {
        
        /// <remarks/>
        public string PromotionName;
        
        /// <remarks/>
        public int UserID;
        
        /// <remarks/>
        public int PlayerID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PlayerIDSpecified;
        
        /// <remarks/>
        public string PlayerCardID;
        
        /// <remarks/>
        public string PromotionNewStatus;
        
        /// <remarks/>
        public System.DateTime StartDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified;
        
        /// <remarks/>
        public System.DateTime EndDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteIDSpecified;
        
        /// <remarks/>
        public string SiteDescription;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRateRestriction
    {
        
        /// <remarks/>
        public int RestrictionID;
        
        /// <remarks/>
        public int RateTypeID;
        
        /// <remarks/>
        public int SiteID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRateType
    {
        
        /// <remarks/>
        public int RateTypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Comp;
        
        /// <remarks/>
        public string Type;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string RateTypeDefault;
        
        public BodyRateType()
        {
            this.Comp = "Y";
            this.Type = "D";
            this.RateTypeDefault = "N";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyReason
    {
        
        /// <remarks/>
        public int ReasonNumber;
        
        /// <remarks/>
        public int ReasonTypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string Status;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyReservationType
    {
        
        /// <remarks/>
        public int ReservationTypeID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public string XRef;
        
        /// <remarks/>
        public string ReservationTypeDefault;
        
        /// <remarks/>
        public string Status;
        
        /// <remarks/>
        public int SiteID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRestrictions
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Restriction")]
        public BodyRestrictionsRestriction[] Restriction;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRestrictionsRestriction
    {
        
        /// <remarks/>
        public string RestrictionName;
        
        /// <remarks/>
        public string RestrictionType;
        
        /// <remarks/>
        public string RestrictionExpiration;
        
        /// <remarks/>
        public string RestrictionComment;
        
        /// <remarks/>
        public string RestrictionNotifyRequired;
        
        /// <remarks/>
        public string RestrictionDisplayStopCode;
        
        /// <remarks/>
        public string RestrictionPostalMailOptOut;
        
        /// <remarks/>
        public string RestrictionEmailOptOut;
        
        /// <remarks/>
        public string RestrictionTextMessagingOptOut;
        
        /// <remarks/>
        public string RestrictionPhoneOptOut;
        
        /// <remarks/>
        public string RestrictionEGMMessaging;
        
        /// <remarks/>
        public string RestrictedActions;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRoomRestriction
    {
        
        /// <remarks/>
        public System.DateTime Date;
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public int RestrictionID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyRoomType
    {
        
        /// <remarks/>
        public int RoomTypeID;
        
        /// <remarks/>
        public string Code;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        public decimal Charge;
        
        /// <remarks/>
        public string RoomTypeDefault;
        
        /// <remarks/>
        public int SiteID;
        
        public BodyRoomType()
        {
            this.RoomTypeDefault = "N";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodySiteInfo
    {
        
        /// <remarks/>
        public int SiteID;
        
        /// <remarks/>
        public string Description;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Abbreviation;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> Code;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodeSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> SiteGroupID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SiteGroupIDSpecified;
        
        /// <remarks/>
        public string Status;
        
        public BodySiteInfo()
        {
            this.Status = "A";
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyStopCodes
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Filter", typeof(BodyStopCodesFilter))]
        [System.Xml.Serialization.XmlElementAttribute("PropertyStopCodes", typeof(BodyStopCodesPropertyStopCodes))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyStopCodesFilter
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SiteID", IsNullable=false)]
        public int[] Sites;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyStopCodesPropertyStopCodes
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Code")]
        public BodyStopCodesPropertyStopCodesCode[] Code;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyStopCodesPropertyStopCodesCode
    {
        
        /// <remarks/>
        public object StopCodeID;
        
        /// <remarks/>
        public object Description;
        
        /// <remarks/>
        public object Priority;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripComment
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> ReservationID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReservationIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ReservationXRef;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> CommentID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CommentIDSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CommentXRef;
        
        /// <remarks/>
        public object Confirmation;
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> Date;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Comment;
        
        /// <remarks/>
        public User User;
        
        /// <remarks/>
        public object CRMGUID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformation
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TripDetail", typeof(BodyTripInformationTripDetail))]
        [System.Xml.Serialization.XmlElementAttribute("TripFilter", typeof(BodyTripInformationTripFilter))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetail
    {
        
        /// <remarks/>
        public int TripNumber;
        
        /// <remarks/>
        public System.DateTime BeginDate;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BeginDateSpecified;
        
        /// <remarks/>
        public int Days;
        
        /// <remarks/>
        public decimal ActualIncome;
        
        /// <remarks/>
        public decimal TheoIncome;
        
        /// <remarks/>
        public int PointsEarned;
        
        /// <remarks/>
        public decimal TheoCompGuideline;
        
        /// <remarks/>
        public decimal TotalExpAsPercentTheo;
        
        /// <remarks/>
        public decimal ActualCompGuideline;
        
        /// <remarks/>
        public decimal TotalExpAsPercentActual;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PromotionID")]
        public string[] Promotions;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Room", IsNullable=false)]
        public BodyTripInformationTripDetailRoom[] RoomInfo;
        
        /// <remarks/>
        public BodyTripInformationTripDetailSlotStatistics SlotStatistics;
        
        /// <remarks/>
        public BodyTripInformationTripDetailTableStatistics TableStatistics;
        
        /// <remarks/>
        public BodyTripInformationTripDetailRevenue Revenue;
        
        /// <remarks/>
        public BodyTripInformationTripDetailExpenses Expenses;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailRoom
    {
        
        /// <remarks/>
        public object RoomNumber;
        
        /// <remarks/>
        public object RoomStatus;
        
        /// <remarks/>
        public object CompStatus;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailSlotStatistics
    {
        
        /// <remarks/>
        public decimal CoinIn;
        
        /// <remarks/>
        public decimal CoinOut;
        
        /// <remarks/>
        public decimal Jackpots;
        
        /// <remarks/>
        public decimal ActWin;
        
        /// <remarks/>
        public decimal TheoWin;
        
        /// <remarks/>
        public decimal XCUsed;
        
        /// <remarks/>
        public int PointsEarned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailTableStatistics
    {
        
        /// <remarks/>
        public decimal AvgBet;
        
        /// <remarks/>
        public string TimePlayed;
        
        /// <remarks/>
        public decimal FourHrConv;
        
        /// <remarks/>
        public decimal CashBuyIn;
        
        /// <remarks/>
        public decimal NetMarkers;
        
        /// <remarks/>
        public decimal ActWin;
        
        /// <remarks/>
        public decimal TheoWin;
        
        /// <remarks/>
        public int PointsEarned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailRevenue
    {
        
        /// <remarks/>
        public decimal ActWin;
        
        /// <remarks/>
        public decimal TheoWin;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailExpenses
    {
        
        /// <remarks/>
        public decimal TotalExpenses;
        
        /// <remarks/>
        public decimal CompLiability;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Expense", IsNullable=false)]
        public BodyTripInformationTripDetailExpensesExpense[] ExpenseEntries;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripDetailExpensesExpense
    {
        
        /// <remarks/>
        public string Type;
        
        /// <remarks/>
        public decimal Amount;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripInformationTripFilter
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public bool IncludeLinks;
        
        /// <remarks/>
        public int LastTrip;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastTripSpecified;
        
        /// <remarks/>
        public int MaxTrips;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxTripsSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummary
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Summary", typeof(BodyTripSummarySummary))]
        [System.Xml.Serialization.XmlElementAttribute("TripFilter", typeof(BodyTripSummaryTripFilter))]
        public object[] Items;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummary
    {
        
        /// <remarks/>
        public decimal ActualIncome;
        
        /// <remarks/>
        public decimal TheoIncome;
        
        /// <remarks/>
        public int PointsEarned;
        
        /// <remarks/>
        public decimal TheoCompGuideline;
        
        /// <remarks/>
        public decimal TotalExpAsPercentTheo;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalExpAsPercentTheoSpecified;
        
        /// <remarks/>
        public decimal ActualCompGuideline;
        
        /// <remarks/>
        public decimal TotalExpAsPercentActual;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalExpAsPercentActualSpecified;
        
        /// <remarks/>
        public BodyTripSummarySummarySlotStatistics SlotStatistics;
        
        /// <remarks/>
        public BodyTripSummarySummaryTableStatistics TableStatistics;
        
        /// <remarks/>
        public BodyTripSummarySummaryRevenue Revenue;
        
        /// <remarks/>
        public BodyTripSummarySummaryExpenses Expenses;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public BodyTripSummarySummarySummaryType SummaryType;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummarySlotStatistics
    {
        
        /// <remarks/>
        public decimal CoinIn;
        
        /// <remarks/>
        public decimal CoinOut;
        
        /// <remarks/>
        public decimal Jackpots;
        
        /// <remarks/>
        public decimal ActWin;
        
        /// <remarks/>
        public decimal TheoWin;
        
        /// <remarks/>
        public decimal XCUsed;
        
        /// <remarks/>
        public int PointsEarned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummaryTableStatistics
    {
        
        /// <remarks/>
        public decimal AvgBet;
        
        /// <remarks/>
        public string TimePlayed;
        
        /// <remarks/>
        public decimal FourHrConv;
        
        /// <remarks/>
        public decimal CashBuyIn;
        
        /// <remarks/>
        public decimal NetMarkers;
        
        /// <remarks/>
        public decimal ActWin;
        
        /// <remarks/>
        public decimal TheoWin;
        
        /// <remarks/>
        public int PointsEarned;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummaryRevenue
    {
        
        /// <remarks/>
        public object ActWin;
        
        /// <remarks/>
        public object TheoWin;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummaryExpenses
    {
        
        /// <remarks/>
        public object TotalExpenses;
        
        /// <remarks/>
        public object CompLiability;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Expense", IsNullable=false)]
        public BodyTripSummarySummaryExpensesExpense[] ExpenseEntries;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummarySummaryExpensesExpense
    {
        
        /// <remarks/>
        public string Type;
        
        /// <remarks/>
        public decimal Amount;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum BodyTripSummarySummarySummaryType
    {
        
        /// <remarks/>
        CDay,
        
        /// <remarks/>
        Curr,
        
        /// <remarks/>
        MTD,
        
        /// <remarks/>
        YTD,
        
        /// <remarks/>
        LTD,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyTripSummaryTripFilter
    {
        
        /// <remarks/>
        public SiteType Site;
        
        /// <remarks/>
        public bool IncludeLinks;
        
        /// <remarks/>
        public int LastTrip;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastTripSpecified;
        
        /// <remarks/>
        public int MaxTrips;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxTripsSpecified;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyUnMerge
    {
        
        /// <remarks/>
        public int VictimPlayerID;
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class BodyZipCode
    {
        
        /// <remarks/>
        public int ZipCodeID;
        
        /// <remarks/>
        public string ZipCode;
        
        /// <remarks/>
        public string City;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string State;
        
        /// <remarks/>
        public int CountryID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Suburb;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        [System.ComponentModel.DefaultValueAttribute("Y")]
        public string Verified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> SegmentID;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SegmentIDSpecified;
        
        /// <remarks/>
        public string Status;
        
        public BodyZipCode()
        {
            this.Verified = "Y";
            this.Status = "A";
        }
    }
}
