﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ICU.Default" Async="True" EnableEventValidation="False" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=8">

<html xmlns="http://www.w3.org/1999/xhtml">
<head  runat="server">    
   <meta charset="utf-8">
    <title>CHER V1</title>
    <link href="Default.css" rel="stylesheet" type="text/css" />
    <!--bottom: 0%; right: 0px; position: absolute;-->
    <script type="text/javascript" >
        function HideLoading() {
            setTimeout(function () {
                document.getElementById('tierLoadingDIV').style.display = 'none'
            }, 10 * 900);
        };       
 
        function OpenHelpLink() {
            window.open("http://helpdesk.revelentertainment.com/");
        };
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
         <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"/>    
        <div>
            <asp:HiddenField ID="hidCurCustomer" runat="server" />
            <asp:Table ID="Table1" runat="server" Width="99%" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" CssClass="allCells">
                        <asp:Panel ID="top" runat="server" CssClass="TitleFrame" Width="100%" Height="50px">
                            <table width="100%">
                                <tr>
                                    <td style="width: 25%" class="titlesides">
                                        <a id="link" href="javascript:void(0);" onclick="OpenHelpLink();">IT Support</a>
                                    </td>
                                    <td style="width: 50%">I C U&nbsp;&nbsp;&nbsp;V4
                                    </td>
                                    <td style="width: 25%" class="titlesides">Current User:
                                        <asp:Label ID="lblUser" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" CssClass="allCells" HorizontalAlign="Center">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="pnlCust" runat="server" Width="100%" Height="90px"
                                    CssClass="allPanels">
                                    <div class="contextDiv">
                                        <asp:Table ID="Table9" runat="server" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center">
                                                    <asp:Label ID="lblPlayerName" runat="server" CssClass="CustName" />&nbsp&nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblVIP2" runat="server" Text=" VIP " Width="50px"
                                                        CssClass="VIP" Visible="false" />&nbsp
                                                     <asp:Label ID="lblFlag2" runat="server" CssClass="Flag" Visible="false" />
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="CustName">
                                                    <asp:Label ID="lblPhone2" runat="server" CssClass="CustName" />&nbsp&nbsp
                                                </asp:TableCell>
                                                <asp:TableCell HorizontalAlign="Center">
                                                    <asp:Image ID="imgCard" runat="server" Height="75px" />&nbsp&nbsp
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Width="10%" Height="600px" CssClass="allCells">
                        <!-- Collapsing panel left pane-->
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="side" runat="server" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <div id="divSearch">
                                        <asp:Panel ID="pnlSearch" runat="server" CssClass="collapsePanelHeader" Height="30px">
                                            <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
                                                <div style="float: left;">Search</div>
                                                <div style="float: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgButton1" runat="server" ImageUrl="~/images/expand_blue.jpg" AlternateText="(Show Details...)" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlSearchChoice" runat="server" CssClass="collapsePanel" Height="0">
                                            <asp:LinkButton ID="lbSearch" runat="server" OnClick="lbSearch_Click">Search Panel</asp:LinkButton>
                                        </asp:Panel>
                                        <cc1:CollapsiblePanelExtender ID="cpeSearch" runat="Server"
                                            TargetControlID="pnlSearchChoice"
                                            ExpandControlID="pnlSearch"
                                            CollapseControlID="pnlSearch"
                                            Collapsed="False"
                                            ImageControlID="imgButton1"
                                            ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg"
                                            SuppressPostBack="true"
                                            SkinID="CollapsiblePanelDemo" />
                                    </div>

                                    <div id="divProfile">
                                        <asp:Panel ID="pnlProfile" runat="server" CssClass="collapsePanelHeader" Height="30px">
                                            <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
                                                <div style="float: left;">Profile</div>
                                                <div style="float: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgButton2" runat="server" ImageUrl="~/images/expand_blue.jpg" AlternateText="(Show Details...)" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlProfileChoice" runat="server" CssClass="collapsePanel" Height="0">
                                            <asp:LinkButton ID="lbProfile" runat="server"
                                                OnClick="lbProfile_Click">Customer Profile</asp:LinkButton><br />
                                            <asp:LinkButton ID="lbProfileDet" runat="server"
                                                OnClick="lbProfileDet_Click">Customer Profile Detail</asp:LinkButton><br />
                                            <asp:LinkButton ID="lbTierSumm" runat="server"
                                                OnClick="lbTierSumm_Click">Tier Summary</asp:LinkButton>
                                        </asp:Panel>

                                        <cc1:CollapsiblePanelExtender ID="cpeProfile" runat="Server"
                                            TargetControlID="pnlProfileChoice"
                                            ExpandControlID="pnlProfile"
                                            CollapseControlID="pnlProfile"
                                            Collapsed="False"
                                            ImageControlID="imgButton2"
                                            ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg"
                                            SuppressPostBack="true"
                                            SkinID="CollapsiblePanelDemo" />
                                    </div>

                                    <div id="divComps">
                                        <asp:Panel ID="pnlComps" runat="server" CssClass="collapsePanelHeader" Height="30px">
                                            <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
                                                <div style="float: left;">Comps</div>
                                                <div style="float: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgButton3" runat="server" ImageUrl="~/images/expand_blue.jpg" AlternateText="(Show Details...)" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCompsChoice" runat="server" CssClass="collapsePanel" Height="0">
                                             <asp:LinkButton ID="lbCompHistory" runat="server" 
                                                OnClick="lbCompHistory_Click">Comp History</asp:LinkButton><br />
                                            <asp:LinkButton ID="lbOffers" runat="server"
                                                OnClick="lbOffers_Click">Offers</asp:LinkButton><br />
                                            <asp:LinkButton ID="lbPromoCodes" runat="server"
                                                OnClick="lbPromoCodes_Click">Promotion Codes</asp:LinkButton>
                                        </asp:Panel>

                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="Server"
                                            TargetControlID="pnlCompsChoice"
                                            ExpandControlID="pnlComps"
                                            CollapseControlID="pnlComps"
                                            Collapsed="False"
                                            ImageControlID="imgButton3"
                                            ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg"
                                            SuppressPostBack="true"
                                            SkinID="CollapsiblePanelDemo" />
                                    </div>

                                    <div id="divHotel">
                                        <asp:Panel ID="pnlHotel" runat="server" CssClass="collapsePanelHeader" Height="30px">
                                            <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
                                                <div style="float: left;">Hotel</div>
                                                <div style="float: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgButton4" runat="server" ImageUrl="~/images/expand_blue.jpg" AlternateText="(Show Details...)" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlHotelChoice" runat="server" CssClass="collapsePanel" Height="0">
                                            <asp:LinkButton ID="lbReservations" runat="server"
                                                OnClick="lbReservations_Click">Guest Reservations</asp:LinkButton><br />
                                        </asp:Panel>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                                            TargetControlID="pnlHotelChoice"
                                            ExpandControlID="pnlHotel"
                                            CollapseControlID="pnlHotel"
                                            Collapsed="False"
                                            ImageControlID="imgButton4"
                                            ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg"
                                            SuppressPostBack="true"
                                            SkinID="CollapsiblePanelDemo" />
                                    </div>

                                    <div id="divGaming">
                                        <asp:Panel ID="pnlGaming" runat="server" CssClass="collapsePanelHeader" Height="30px">
                                            <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
                                                <div style="float: left;">Gaming</div>
                                                <div style="float: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgButton5" runat="server" ImageUrl="~/images/expand_blue.jpg" AlternateText="(Show Details...)" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlGamingChoice" runat="server" CssClass="collapsePanel" Height="0">                                          
                                            <asp:LinkButton ID="lbComments" runat="server"
                                                OnClick="lbComments_Click">Player Comments</asp:LinkButton><br />                                              
                                             <asp:LinkButton ID="lbEval" runat="server"
                                                OnClick="lbEval_Click">Player Eval</asp:LinkButton><br />
                                            <asp:LinkButton ID="lbCredit" runat="server" Visible="false"
                                                OnClick="lbCredit_Click">Player Credit</asp:LinkButton><br />
                                        </asp:Panel>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="Server"
                                            TargetControlID="pnlGamingChoice"
                                            ExpandControlID="pnlGaming"
                                            CollapseControlID="pnlGaming"
                                            Collapsed="False"
                                            ImageControlID="imgButton5"
                                            ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg"
                                            SuppressPostBack="true"
                                            SkinID="CollapsiblePanelDemo" />
                                      
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:TableCell>

                    <asp:TableCell Width="80%" Height="600px" CssClass="allCells">
                        <!-- Main content panes-->

                        <asp:UpdatePanel ID="MainUpdatePanel" runat="server" >
                            <ContentTemplate>
                                <asp:Panel ID="pnlProfileMain" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <div align="center">
                                        <asp:Table ID="Table2" runat="server" Width="50%" CssClass="leftTable">
                                            <asp:TableRow VerticalAlign="Top">
                                                <asp:TableCell VerticalAlign="Top" ColumnSpan="2" HorizontalAlign="Center">
                                                    <asp:Label ID="lblVIP" runat="server" Width="50px" CssClass="VIP"
                                                        Text="VIP" Visible="false"></asp:Label><br />
                                                    <asp:Label ID="lblCustStatus" runat="server" CssClass="Flag"></asp:Label><br />
                                                    <br />
                                                    <br />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow VerticalAlign="Middle">
                                                <asp:TableCell>
                                                    <fieldset>
                                                        <legend>Profile Info
                                                        </legend>
                                                        <br />
                                                        <table>
                                                            <tr>
                                                                <td>CustID:</td>
                                                                <td>
                                                                    <asp:Label ID="lblCustID" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Title:</td>
                                                                <td>
                                                                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                           <%-- <tr>
                                                                <td>First Name:</td>
                                                                <td>
                                                                    <asp:Label ID="lblFName" runat="server"></asp:Label>
                                                                 </td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td>Name:</td>
                                                                <td>
                                                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                                                 </td>
                                                            </tr>
                                                            <tr>
                                                                <td>DOB:</td>
                                                                <td>
                                                                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Card Tier:</td>
                                                                <td>
                                                                    <asp:Label ID="lblRank" runat="server"></asp:Label>
                                                                 </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Address:</td>
                                                                <td>
                                                                    <asp:Label ID="lblAddr" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Phone:</td>
                                                                <td>
                                                                    <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                                                  </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Preferred E-mail:</td>
                                                                <td>
                                                                    <asp:Label ID="lblPrefEmail" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Secondary E-mail:</td>
                                                                <td>
                                                                    <asp:Label ID="lblEMail" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>FreePlay:</td>
                                                                <td>
                                                                    <asp:Label ID="lblFSP" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Resort Dollars:</td>
                                                                <td>
                                                                    <asp:Label ID="lblRSD" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Host:</td>
                                                                <td>
                                                                    <asp:Label ID="lblHost" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlProfileDetMain" runat="server" Visible="false" width="100%" height="600px"  CssClass="allPanels">
                                    <div align="center">
                                        <asp:Table ID="tblProfileDet" runat="server">
                                            <asp:TableRow ID="tblProfileDetRow">
                                                <asp:TableCell>
                                                    <asp:GridView ID="gvProfileName" runat="server" AutoGenerateColumns="True"
                                                        BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="2px" CellSpacing="3"
                                                        CellPadding="3" EmptyDataText="No data to display" PageSize="5" Font-Names="Tahoma" Font-Size="Small">
                                                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:GridView>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:GridView ID="gvProfileAddress" runat="server" AutoGenerateColumns="True"
                                                        BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="2px" CellSpacing="3"
                                                        CellPadding="3" EmptyDataText="No data to display" PageSize="5" Font-Names="Tahoma" Font-Size="Small">
                                                         <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                    </asp:GridView>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:GridView ID="gvProfilePhone" runat="server" AutoGenerateColumns="True"
                                                        BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="2px" CellSpacing="3"
                                                        CellPadding="3" EmptyDataText="No data to display" PageSize="5" Font-Names="Tahoma" Font-Size="Small">
                                                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:GridView>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:GridView ID="gvProfileIdentity" runat="server" AutoGenerateColumns="True"
                                                        BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="2px" CellSpacing="3"
                                                        CellPadding="3" EmptyDataText="No data to display" PageSize="5" Font-Names="Tahoma" Font-Size="Small">
                                                       <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                    </asp:GridView>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlSearchMain" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <asp:Table ID="Table4" runat="server" Width="50%">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                             Player ID:
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtPlayerID" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Button ID="btnSubmitPlayer" runat="server" Text="Lookup ID" OnClick="btnSubmitPlayer_Click" ValidationGroup="IDValidators" />&nbsp;&nbsp;
                                                <asp:RequiredFieldValidator ControlToValidate="txtPlayerID" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Player ID required." ValidationGroup="IDValidators" />
                                                <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="Player ID must be 8 digits" ControlToValidate="txtPlayerID"
                                                    ValidationExpression="^[0-9]{8}$" ValidationGroup="IDValidators" />
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="False" ValidationGroup="IDValidators" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                             Last Name:
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Button ID="btnNameLookup" runat="server" Text="Lookup Name" ValidationGroup="NameValidators" OnClick="btnNameLookup_Click" />&nbsp;&nbsp;
                                                  <asp:RequiredFieldValidator ControlToValidate="txtLName" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Last name is required." ValidationGroup="NameValidators" />
                                                  <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="false" ShowSummary="False" ValidationGroup="NameValidators" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                             First Name:
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                             Zip Code:
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtSrchZip" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="3">
                                                <asp:Label ID="lblSrchError" runat="server" Visible="false" CssClass="errorLabel"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="3"><br /><br /></asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional"
                                                    ChildrenAsTriggers="true">
                                                    <ContentTemplate>
                                                        <div class="TableDiv">
                                                            <asp:GridView ID="NamesView" runat="server" AutoGenerateColumns="True"
                                                                AutoGenerateSelectButton="True"
                                                                BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                                                CellPadding="3"
                                                                AllowPaging="True" EmptyDataText="No data to display" PageSize="15"
                                                                Font-Names="Tahoma" Font-Size="Small" OnPageIndexChanging="NamesView_PageIndexChanging"
                                                                OnSelectedIndexChanged="NamesView_SelectedIndexChanged">
                                                                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                                <RowStyle BackColor="White" ForeColor="#003399" />
                                                                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>

                                <asp:Panel ID="pnlTierSumm" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">                                       
                                    <div id="tierLoadingDIV" class="updProgress"> Loading...
                                    <img src="Images/LoadingProgressBar.gif" id="tierloading" onload="HideLoading()" /></div>
                                    <iframe id="tierApp" runat="server" width="100%" height="100%" />
                                </asp:Panel>                                 

                                <asp:Panel ID="pnlCompHistory" runat="server" Visible="false" Width="100%" Height="600px" CssClass="allPanels">                                   
                                    <div id="CompHistoryGrid" runat="server" >
                                        <p align="center"><strong>Comp History</strong></p>
                                           <asp:GridView ID="CompHistoryView" runat="server" AutoGenerateColumns="False"  
                                            AutoGenerateSelectButton="False"
                                            AllowSorting="true"
                                            BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                            CellPadding="3"
                                            AllowPaging="True" EmptyDataText="No data to display" PageSize="23"
                                            Width="100%"
                                            Font-Names="Tahoma" Font-Size="Small"
                                            OnPageIndexChanging="CompHistoryView_PageIndexChanging"
                                            OnSorting="CompHistoryView_Sorting"                                           
                                            OnSelectedIndexChanged="CompHistoryView_SelectedIndexChanged"  >                                                                                   
                                            <Columns>   
                                                <asp:TemplateField HeaderText="Comp ID" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "CompID")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" SortExpression="CompDescription"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "CompDescription")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Issued Date" SortExpression="IssueDT"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "IssueDT")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" SortExpression="Status"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Status")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Redeemed Date" SortExpression="PostedDate" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="Covers" >
                                                    <ItemTemplate>                                                            
                                                         <%# DataBinder.Eval(Container.DataItem, "Quantity")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Points"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "PointValue")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comp"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "RetailValue")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rewards"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "DaysValid")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorized By"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "AuthorizerLogin")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="Issued By"  >
                                                    <ItemTemplate>                                                            
                                                         <%# DataBinder.Eval(Container.DataItem, "IssuedBy")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type" SortExpression="FreeCompType" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>                                                            
                                                         <%# DataBinder.Eval(Container.DataItem, "FreeCompType")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                               
                                            </Columns>
                                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                            <RowStyle BackColor="White" ForeColor="#003399" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White" CssClass="sortColumn"></PagerStyle>
                                        </asp:GridView>
                                        <br />
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlOffers" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <div>
                                        <p align="center"><strong>Offers</strong></p>
                                        <asp:GridView ID="OffersView" runat="server" AutoGenerateColumns="False"  
                                            AutoGenerateSelectButton="False"
                                            AllowSorting="true"
                                            BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                            CellPadding="3"
                                            AllowPaging="True" EmptyDataText="No data to display" PageSize="10"
                                            Width="100%"
                                            Font-Names="Tahoma" Font-Size="Small"
                                            OnPageIndexChanging="OffersView_PageIndexChanging"
                                            OnSorting="OffersView_Sorting"
                                            OnRowDataBound="OffersView_RowDataBound"
                                            OnSelectedIndexChanged="OffersView_SelectedIndexChanged"
                                            OnRowCommand="OffersView_RowCommand">                                           
                                            <Columns>                                               
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkEvent" CommandArgument='<%#Eval("PromoID")%>' Text="Select" CommandName="ManageTickets" Visible="false" />                                                       
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>  
                                                <asp:TemplateField HeaderText="Category" SortExpression="Category" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Category")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Promo Name" SortExpression="Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "CMSID")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Long Description">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "LongDescription")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "StartDate", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date" SortExpression="EndDate" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "EndDate", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                    <ItemTemplate>                                                            
                                                        <asp:Label runat="server" ID="lblEventStatus" CommandArgument='<%#Eval("Status")%>' Text='<%# DataBinder.Eval(Container.DataItem, "Status")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Label">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value1.Label")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Value">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value1.Value")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Label">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value2.Label")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Value">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value2.Value")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Label">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value3.Label")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail Value">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Value3.Value")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                            <RowStyle BackColor="White" ForeColor="#003399" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White" CssClass="sortColumn"></PagerStyle>
                                        </asp:GridView>
                                        <br />
                                        <asp:Table runat="server" ID="tblEventAction" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="headerRow" Width="10%">Promo Code</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="10%">Event ID</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="25%">Block</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="5%">Total Available</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%">Max Per Player</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%"># to Reserve</asp:TableCell>                                                
                                                <asp:TableCell CssClass="headerRow" Width="15%" ID="rowAction">Action</asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblPromoID" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblEventID" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="25%">                                                   
                                                    <asp:DropDownList ID="ddlBlocks" runat="server"  OnSelectedIndexChanged="ddlBlocks_SelectedIndexChanged" AutoPostBack="true"/> 
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblTotAvail" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                 <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblMaxPlayer" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                 <asp:TableCell CssClass="dataRow" Width="10%">
                                                   <asp:DropDownList ID="ddlNumTickets" runat="server" />
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="15%">
                                                    <asp:Button ID="btnEventAction" runat="server" OnClick="btnEventAction_Click" Text="Reserve" ></asp:Button>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <br />
                                        <asp:HiddenField ID="hidEventStatus" runat="server"  Visible="false" />
                                        <asp:Label ID="lblEventMessage" runat="server" Visible="false" CssClass="errorLabel"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlPromoCodes" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <div>
                                        <p align="center"><strong>Promotion Codes</strong></p>
                                        <asp:GridView ID="PromoCodesView" runat="server" AutoGenerateColumns="False"
                                            AutoGenerateSelectButton="False"
                                            BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                            CellPadding="3"
                                            AllowPaging="True" EmptyDataText="No data to display" PageSize="10"
                                            Width="100%"
                                            Font-Names="Tahoma" Font-Size="Small"
                                            SelectedRowStyle-BackColor="LightGreen"
                                            OnRowDataBound="PromoCodesView_RowDataBound"
                                            OnSelectedIndexChanged="PromoCodesView_SelectedIndexChanged"
                                            OnPageIndexChanging="PromoCodesView_PageIndexChanging"
                                            OnSorting="PromoCodesView_Sorting">
                                            <Columns>
                                                <asp:CommandField SelectText="Select" ShowSelectButton="True" ButtonType="Link" ControlStyle-CssClass="hlight" />
                                                <asp:TemplateField HeaderText="Promo Code" SortExpression="promoCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpromoCode" runat="server" Text='<%#Eval("PromoCode")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Promo Description" SortExpression="promoDescription">
                                                    <ItemTemplate>
                                                        <%#Eval("PromoDesc")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date" SortExpression="startDate">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "StartDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date" SortExpression="endDate">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "EndDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" Visible="False">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "publicDescription")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Max Nights" SortExpression="maxNights">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "MaxNights")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Redeemed Flag" SortExpression="redeemedFlag">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblPCRedeemFlag" Text='<%#Eval("Redeemed")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Redeemed Date" SortExpression="redeemedDate">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "RedeemDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reservation ID" SortExpression="resID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblPCResID" Text='<%#Eval("ResID")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                            <RowStyle BackColor="White" ForeColor="#003399" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White"></PagerStyle>
                                        </asp:GridView>
                                        <br />
                                        <br />
                                        <asp:Table runat="server" ID="tblPCRedeem" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="headerRow" Width="10%">Promo Code</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="10%">Rate Plan</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="25%">Message</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="5%">Redeemed</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%">Res ID</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%">Checkin</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%">Checkout</asp:TableCell>
                                                <asp:TableCell CssClass="headerRow" Width="15%" ID="thcRedeem">Action</asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblPromoCode" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="10%">
                                                    <asp:Label ID="lblPCRatePlan" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="25%">
                                                    <asp:Label ID="lblPCMsg" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="5%">
                                                    <asp:Label ID="lblPCStatus" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="15%">
                                                    <asp:TextBox ID="txtPCResID" runat="server"></asp:TextBox>                                                   
                                                </asp:TableCell>
                                                <asp:TableCell Width="15%">
                                                    <asp:TextBox ID="txtcheckInDate" runat="server" />
                                                    <cc1:CalendarExtender ID="calExtenderCheckIn" runat="server" CssClass="cal_Theme1" Enabled="true" TargetControlID="txtcheckInDate" />
                                                </asp:TableCell>
                                                <asp:TableCell Width="15%">
                                                    <asp:TextBox ID="txtcheckOutDate" runat="server" />
                                                    <cc1:CalendarExtender ID="calExtenderCheckOut" runat="server" CssClass="cal_Theme1" Enabled="true" TargetControlID="txtcheckOutDate" />
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="dataRow" Width="15%">
                                                    <asp:Button ID="btnPCAction" runat="server" Text="Redeem" OnClick="btnPCAction_Click" Visible="false" ValidationGroup="ResIDValidators"></asp:Button>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <br />
                                        <br />
                                        <asp:Button ID="btnClearPromo" runat="server" Text="Clear" OnClick="btnClearPromo_Click" Visible="false" />
                                        <br />
                                         <br />
                                        <asp:Label ID="lblPCInfoMessage" runat="server" Visible="false" CssClass="errorLabel"></asp:Label><br />
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="ResID must be 12 digits.  No spaces or characters are permitted." ControlToValidate="txtPCResID"
                                                    ValidationExpression="^[0-9]{12}$" ValidationGroup="ResIDValidators" CssClass="errorLabel" />                                       
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="false" ShowSummary="False" ValidationGroup="ResIDValidators" />
                                    </div>
                                </asp:Panel>

                                 <asp:Panel ID="pnlReservationsMain" runat="server" Visible="false" Width="100%" Height="600px" CssClass="allPanels">   
                                     <div>
                                         <table id="tblReservationsCalendar" runat="server">
                                             <tr>
                                                 <td><b>Start Date: </b>
                                                     <asp:TextBox ID="txtResStartDate" runat="server" />
                                                    <cc1:CalendarExtender ID="cc1ResStart" runat="server"  CssClass="cal_Theme1" Enabled="true" TargetControlID="txtResStartDate" />                                                    
                                                 </td>
                                                 <td><b>End Date: </b>
                                                     <asp:TextBox ID="txtResEndDate" runat="server" />
                                                     <cc1:CalendarExtender ID="cc1ResEnd" runat="server" CssClass="cal_Theme1" Enabled="true" TargetControlID="txtResEndDate" />
                                                 </td>                                                    
                                                  <td><asp:Button ID="btnReservationSubmit" runat="server" Text="Submit"  OnClick="btnReservationSubmit_Click" ></asp:Button></td>
                                             </tr>
                                         </table>                                       
                                        <br />
                                        <asp:Label ID="lblReservationMessage" runat="server" Visible="false" CssClass="errorLabel"></asp:Label>
                                     </div>                        
                                     <div>
                                         <p align="center"><strong>Guest Reservations</strong></p>
                                         <asp:GridView ID="ReservationsView" runat="server" AutoGenerateColumns="False"
                                             AutoGenerateSelectButton="False"
                                             AllowSorting="true"
                                             BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                             CellPadding="3"
                                             AllowPaging="True" EmptyDataText="No data to display" PageSize="10"
                                             Width="100%"
                                             Font-Names="Tahoma" Font-Size="Small"
                                             OnPageIndexChanging="ReservationsView_PageIndexChanging"
                                             OnSorting="ReservationsView_Sorting"
                                             OnSelectedIndexChanged="ReservationsView_SelectedIndexChanged">
                                             <Columns>
                                                 <asp:TemplateField HeaderText="Reservation ID">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "KeyDate") %><%# DataBinder.Eval(Container.DataItem, "Seq") %>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Reservation Status" SortExpression="ResStatus" HeaderStyle-CssClass="sortColumn">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "ResStatus")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Rate Plan" SortExpression="RatePlan" HeaderStyle-CssClass="sortColumn">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "RatePlan")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Arrival Date" SortExpression="ArrivalDT" HeaderStyle-CssClass="sortColumn">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "ArrivalDT", "{0:MM/dd/yyyy}")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Departure Date" SortExpression="DepartureDT" HeaderStyle-CssClass="sortColumn">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "DepartureDT", "{0:MM/dd/yyyy}")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Room #">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "RoomNum")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Room Type">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "RoomType")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Confirmation #">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "ConfNum")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Cancellation #">
                                                     <ItemTemplate>
                                                         <%# DataBinder.Eval(Container.DataItem, "CxlNum")%>
                                                     </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                 </asp:TemplateField>
                                             </Columns>
                                             <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                             <RowStyle BackColor="White" ForeColor="#003399" />
                                             <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                             <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White" CssClass="sortColumn"></PagerStyle>
                                         </asp:GridView>
                                         <br />
                                     </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlEvalMain" runat="server" Visible="false" Width="100%" Height="600px" CssClass="allPanels">   
                                    <div>
                                        <table id="tblEvalSelection" runat="server">
                                            <tr>
                                                <td><b>Eval Period: </b></td>    
                                                <td>
                                                    <asp:DropDownList ID="ddlEvalPeriod" runat="server">
                                                        <asp:ListItem Enabled="true" Text="Today" Value="1"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Last Trip" Value="2"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="This Month" Value="3"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="This Year" Value="4"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Previous Trip" Value="5"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Last Month" Value="6"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Last Year" Value="7"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Lifetime" Value="8"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="This Quarter" Value="9"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Last 90 Days" Value="10" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Enabled="true" Text="Last Quarter" Value="11"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <hr />
                                                </td>
                                                <td>
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Eval Type: </b></td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblEvalType" runat="server" RepeatDirection="Vertical" TextAlign="Left">
                                                        <asp:ListItem Enabled="true" Text="Total" Value="1" Selected="True" />
                                                        <asp:ListItem Enabled="true" Text="Trip Average" Value="2" />
                                                        <asp:ListItem Enabled="true" Text="Day Average" Value="3" />
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <hr />
                                                </td>
                                                <td>
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnEval" runat="server" Text="Submit" OnClick="btnEval_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <asp:Label ID="lblEvalMessage" runat="server" Visible="false" CssClass="errorLabel"></asp:Label>
                                    </div>                                        
                                    <div>   
                                         <p><strong>Player Eval</strong></p>                                                                             
                                        <span id="txtEvalHTML"  runat="server"  class="eval" />  
                                    </div>
                                </asp:Panel>

                                  <asp:Panel ID="pnlCommentsMain" runat="server" Visible="false" Width="100%" Height="600px" CssClass="allPanels">                                   
                                    <div>
                                        <p align="center"><strong>Player Comments</strong></p>
                                           <asp:GridView ID="CommentsView" runat="server" AutoGenerateColumns="False"  
                                            AutoGenerateSelectButton="False"
                                            AllowSorting="true"
                                            BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                            CellPadding="3"
                                            AllowPaging="True" EmptyDataText="No data to display" PageSize="23"
                                            Width="100%"
                                            Font-Names="Tahoma" Font-Size="Small"                                            
                                            OnPageIndexChanging="CommentsView_PageIndexChanging"
                                            OnSorting="CommentsView_Sorting"                                           
                                            OnSelectedIndexChanged="CommentsView_SelectedIndexChanged"  >                                                                                   
                                            <Columns>
                                              <asp:TemplateField HeaderText="Comment #" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "CommentNum")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comment" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Comment")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Entered Date" SortExpression="EnteredDT"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "EnteredDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Expiration Date" SortExpression="ExpirationDT" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "ExpirationDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>  
                                                <asp:TemplateField HeaderText="Visibility" SortExpression="CommentType"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "CommentType")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                                                                            
                                                <asp:TemplateField HeaderText="Entered By" SortExpression="EnteredBy" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>                                                            
                                                         <%# DataBinder.Eval(Container.DataItem, "EnteredBy")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Priority" SortExpression="Priority" HeaderStyle-CssClass="sortColumn" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Priority")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Private"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Private")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Status")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                                   
                                            </Columns>
                                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                            <RowStyle BackColor="White" ForeColor="#003399" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White" CssClass="sortColumn"></PagerStyle>
                                        </asp:GridView>
                                        <br />
                                    </div>
                                </asp:Panel>
                                  
                                 <asp:Panel ID="pnlCreditMain" runat="server" Visible="false" Width="100%" Height="600px" CssClass="allPanels">                                   
                                    <div>
                                        <p align="center"><strong>Player Credit</strong></p>
                                           <asp:GridView ID="CreditView" runat="server" AutoGenerateColumns="False"  
                                            AutoGenerateSelectButton="False"
                                            AllowSorting="true"
                                            BackColor="White" BorderColor="#3366CC" BorderStyle="Groove" BorderWidth="1px"
                                            CellPadding="3"
                                            AllowPaging="True" EmptyDataText="No data to display" PageSize="10"
                                            Width="100%"
                                            Font-Names="Tahoma" Font-Size="Small"                                            
                                            OnPageIndexChanging="CreditView_PageIndexChanging"
                                            OnSorting="CreditView_Sorting"                                           
                                            OnSelectedIndexChanged="CreditView_SelectedIndexChanged" >                                                                                   
                                            <Columns>
                                              <asp:TemplateField HeaderText="Site" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "SiteName")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Account" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "PlayerID")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="Primary" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Primary")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                                                                            
                                                <asp:TemplateField HeaderText="Credit Line" >
                                                    <ItemTemplate>                                                            
                                                         <%# DataBinder.Eval(Container.DataItem, "CreditLine", "{0:0.00}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Outstanding" >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Outstanding", "{0:0.00}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Available", "{0:0.00}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stop Codes"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "StopCodes")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="Front Money"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "FrontMoney", "{0:0.00}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TTO"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "TTO")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>  
                                                 <asp:TemplateField HeaderText="In Transit"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "InTransit", "{0:0.00}")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>   
                                                <asp:TemplateField HeaderText="Last Marker"  >
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "LastMarker")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>                                               
                                                <%-- <asp:TemplateField HeaderText="Entered Date" SortExpression="EnteredDT"  HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "EnteredDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Expiration Date" SortExpression="ExpirationDT" HeaderStyle-CssClass="sortColumn">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "ExpirationDT", "{0:MM/dd/yyyy}")%>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>          --%>                                         
                                            </Columns>
                                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                            <RowStyle BackColor="White" ForeColor="#003399" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <PagerStyle HorizontalAlign="Left" BackColor="Black" ForeColor="White" CssClass="sortColumn"></PagerStyle>
                                        </asp:GridView>
                                        <br />
                                    </div>
                                </asp:Panel>
                                  

                                <asp:Panel ID="pnlException" runat="server" Visible="false" Width="100%" Height="600px"
                                    CssClass="allPanels">
                                    <p>
                                        <strong>ERROR</strong><br />
                                        <asp:Label ID="lblException" runat="server" CssClass="errorLabel"></asp:Label>
                                    </p>

                                </asp:Panel>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" >
                            <ProgressTemplate>
                                Loading...
                                <img alt="" src="Images/LoadingProgressBar.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        &nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
<script type="text/javascript">
    function ShowWait() {
        parent.alert("Please Wait While Player Tier is Loading...");
    };
</script>
